# ML Report

Written elaboration on the machine learning part of the research project [Machine Learning DNS (MLDNS)](https://gitlab.com/mldns/mldns).

## Machine Learning DNS - Making home networks secure with AI

The fully build PDF file can be found [here](https://mldns.gitlab.io/ml-report/ml-report.pdf).

This report focuses mainly on the machine learning / AI aspect of the research project [Machine Learning DNS (MLDNS)](https://gitlab.com/mldns/mldns).

The two underlying techniques used and compared to classify domain names into benign and malicious are TensorFlow and fastText. In the repositories under the same names, [TensorFlow](https://gitlab.com/mldns/tensorflow) and [FastText](https://gitlab.com/mldns/fasttext), you can find the underlying experiments described in this report.


## Setup

Setup report environment.

```bash
python -m venv .venv
pip install -U pip
pip install .
./scripts/merge_tf_data.py
./scripts/create_plots.py
./scripts/create_tables.py
```
