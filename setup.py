#!/usr/bin/env python
############################################################
# Setup script.
############################################################
# Install requirements.
############################################################

from setuptools import setup


__version__ = '0.0.1'


setup(
    name='report',
    version=__version__,
    #author='',
    #author_email='',
    url='https://gitlab.com/mldns/ml-report',
    description='Written elaboration about the machine learning of the research project.',
    #long_description='...',
    #extras_require={'test': 'pytest'},
    install_requires=[
        'setuptools>=42',
        'numpy',
        'matplotlib',
        'pandas',
        'requests',
        'pygments',
    ],
)
