\section{Discussion}\label{sec:discussion}

In the following, we will discuss our results.
First, we compare both experiments, \textit{FastText} and \textit{TensorFlow}.
Afterwards, we will state a model recommendation based on our results.
Additionally, we will highlight some particularities and share an experience report of an real life experiment.


\subsection{FastText vs. TensorFlow}

A major point of comparison between TensorFlow and FastText is that FastText acts solely on CPUs and does not require a GPU.
TensorFlow can also act only on CPUs, but the increase in training time becomes intolerable.
In this case FastText has the advantage that it only needs $5$ minutes.
In contrast, the TensorFlow models took several hours.
However, there were also models that were trained relatively quickly with TensorFlow, such as the \textit{Basic} model, which also only needed approx. $5$ minutes, but with the help of a GPU.

If we look at the metrics of the results of each experiment, FastText~\ref{sec:ft-results} and TensorFlow~\ref{sec:tf-results}, we can see that FastText trains more successful models.
Although, the best TensorFlow model has great results, with an accuracy of $0.946$ and a well dispersion of all metric scores, it can still be seen that this model would not appear in the top $10$ of the FastText models.

In the next figure~\ref{fig:ft-vs-tf:metrics}, the metrics for the four best models of FastText and TensorFlow can be compared.
This figure clearly shows once again that the FastText models are superior.
The only model that comes close is the best TensorFlow model in run \texttt{dd25230}.

It is also interesting to note that the distribution pattern of the metrics in both experiments are basically identical.
The metrics \textit{bad precision} and \textit{top recall} are mostly below and the metrics \textit{top precision} and \textit{bad recall} above the \textit{accuracy} value.

Furthermore, a large variance between the individual metrics can be recognized in TensorFlow.
Only the best model shows a similarly good distribution as the FastText models.

All in all, FastText trains the more successful models.

\begin{figure}[H]
    \subfloat[FastText\label{fig:ft-vs-tf:metrics:a}]{%
        \includesvg[width=.49\textwidth]{plots/discussion/ft_metrics_top_4.svg}
    }
    \hspace*{\fill}
    \subfloat[TensorFlow\label{fig:ft-vs-tf:metrics:b}]{%
        \includesvg[width=.49\textwidth]{plots/discussion/tf_metrics_top_4.svg}
    }
	\caption{FastText vs. TensorFlow: Metrics - Top $4$}
	\label{fig:ft-vs-tf:metrics}
\end{figure}

\newpage
In figure~\ref{fig:ft-vs-tf-model-size}, you can see that TensorFlow trains the smaller models.
The median size of FastText models is around $190$~MB.
In contrast, TensorFlow has a median size of only $10$~MB.

\begin{figure}[H]
    \centering
    \includesvg[width=.6\textwidth]{plots/discussion/compare_model_size_in_mb_boxplot.svg}
    \caption{FastText vs. TensorFlow: Model Sizes}
	\label{fig:ft-vs-tf-model-size}
\end{figure}

It should be mentioned that fastText provides a feature to further compress trained models.
In this project we have decided not to use this feature.
Therefore, all data about the model size in the FastText experiment are not further compressed.

The model compression feature is called quantization at fastText.
fastText advertises that a $350$~MB model can be reduced with quantization to a file size of less than $1$~MB.
At this point it should be emphasized that the resulting model loses significantly in quality (accuracy etc.) if the quantization is chosen too strongly.


\subsection{Recommendation}

Based on the results and the comparison between TensorFlow and FastText we recommend to use one of the FastText models.
First, the models can be trained quickly.
On the other hand, the results of the various metrics are of higher quality than with the TensorFlow models.
The only weak point of the FastText models is the larger model size.
However, this weakness could be compensated by quantization.

We recommend the fourth model (\texttt{d90b78f}).
The very slightly worse performance (\ref{table:fasttext-metrics-10}) compared to the top $3$ is negligible.
In return, it has a relatively small model size of only $61.488$~MB.

In addition, because FastText can be efficiently trained on CPUs, this choice enables potentially important features such as online learning (\ref{sec:online-learning}).


\newpage
\subsection{Particularities}

An unexpected highlight of this project was to see how well an artificial intelligence can differentiate malicious domains from benign ones just by the domain name.
This has by far exceeded our expectations.

To illustrate how well the recommended model can distinguish between malicious (Bad) and other (Top) domains, we created the following plot.
A $1$ symbolizes the $100\%$ certainty of the model that the predicted domain is good.
Accordingly, a $-1$ symbolizes the $100\%$ certainty of the model that the predicted domain is malicious.
Red dots always correspond to good domains and black dots to malicious domains.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{discussion/ft-1d-d90b78f.png}
    \caption{Test results of recommended model}
    \label{fig:recommended-model-plot}
\end{figure}

This plot can be found in the \hyperref[apx:ft:plots]{appendix} for all top $10$ models of the FastText experiment.


\subsection{Experience Report}

In the Telecom Challenge, we also developed a finished DNS resolver prototype with Unbound~\cite{unbound}, which integrates the proposed concept with a FastText model.
This concept was tested by our team colleague \href{mailto:leon_gerrit.lenzen@smail.th-koeln.de}{Léon Lenzen} in a two-person household over a two-week period under real conditions.

The setup of the experiment was as follows.
A Raspberry Pi~4 contained the aforementioned DNS resolver prototype, which was connected to a FritzBox router via Gigabit Ethernet.
In addition, the FritzBox was set up to distribute the Raspberry Pi's IP address as DNS server.

In total, $300,852$ predictions were made of which $299,842$ ($99.66\%$) were good, and $1,010$ ($0.34\%$) were categorized as malicious.
From the $1,010$ predictions, around $20$ had an actual impact on user experience.
Blocked subdomains caused all these impacts.
The three most impactful were WhatsApp Web, payments on bahn.de, and warnings via NINA.

From the user experience perspective, no change in response time was notable compared to the default DNS forwarder used in a FritzBox.
The prediction time was constantly around one millisecond.
The average prediction time for benign domains was $1.109$~ms and $1.064$~ms for malicious domains.

Almost all requests ($99.40\%$) were either \texttt{A} or \texttt{AAAA} records.

Overall, the feedback from the test users was inconspicuous.
The test users did not experience any noteworthy degradation in user experience.
Most of the normal Internet traffic was unaffected.
One person encountered problems with some cryptic looking company-internal domains, which was quickly fixed by a wildcard entry to the personal whitelist.
