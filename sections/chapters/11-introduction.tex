\section{Introduction}

Cyber crime is one of the most important concerns in our all-digital society.
The number of attacks conducted is continuously growing and has exploded during the Covid-$19$ pandemic.
Besides corporate targets, also home networks and smartphones are in scope of attacks, especially for phishing, identity theft, botnets, cryptojacking and ransomware.

Below we have gathered some statistics published by PurpleSec LLC~\cite{PurpleSec}, SonicWall~\cite{SonicWall} and David Braue~\cite{Braue}.
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{introduction/statistics-small.jpg}
    \caption{Statistics}
    \label{fig:statistics}
\end{figure}

Today, few measures are in place in home networks to protect users.
Anti-virus software provides limited protection but are mostly available for systems with widely used operating systems.

Standard routers typically come with NAT or a firewall to protect the user from incoming requests from the web.
More advanced device features integrated firewalls that allow filtering IP addresses or address ranges.
Some manufacturers even manage and distribute lists of known malicious IP addresses and provide them to their customers.
Filtering is commonly based on IP addresses, which can be subject to change.
Especially hackers have an interest in frequently changing their IP addresses to undermine IP filtering.
As categorizing IP addresses automatically with algorithms is difficult, such features are hardly found in consumer-grade equipment.
Existing cybersecurity solutions for home networks are almost entirely focusing on the devices they are running of:
In particular, for well-known desktop operating systems, antivirus software has been a significant market for the past $30$ years.

Due to changing attack scenarios, existing security products are challenged.
With new technology, also new solutions have to be found.
The way people interact with their devices has changed dramatically:
Sharing one computer with a family is not the norm anymore.
The primary device for browsing the Internet these days is a smartphone or tablet, which is always connected and stores a lot of very personal and sensitive information, which makes them an attractive target for attacks.
The number of connected devices in homes will keep increasing over the next years.

It is time to think big:
To think of solutions that protect the whole network, not just individual devices.

One of such a solution is the centralized IP filtering mechanism provided by the ISPs.
This IP filtering solution involves much manual work, since traffic has to be analyzed, and potential malicious domain or IP addresses need to be identified.
Also, the users must comply to the filtering as they are not able to do any manual changes.
Therefore, the ISPs have to double-check their evaluation to ensure that only IPs clearly involved in malicious activities are blocked.
Also, regular reevaluations are necessary, as IP assignments can change over time and potentially harmless services could use blocked IP addresses formerly involved in cybercrime.
Our idea is to perform filtering already in the users' local network.
This filtering is done using domain names and machine learning, which is why this project is called Machine Learning DNS~(MLDNS).
MLDNS should filter traffic at the home network level by using an underlying artificial intelligence~(AI), which filters the domains automatically for the user.
This reduces the manual workload of the ISP for their filtering mechanism.
The users also gain the possibility to override these evaluations.

The aim of the research was to investigate, as part of the Telekom Challenge, whether it is possible to use machine learning and artificial intelligence to detect whether a domain is malicious based on its name alone.


\subsection{Document Structure}

The structure of this research work is based on the individual phases we went through during the Telekom Challenge.

First of all, we familiarized ourselves with the topic and searched for and followed up on various \textit{existing~approaches}~(\ref{sec:existing-approaches}).
Based on these findings, we then developed our own \textit{proposed~concept}~(\ref{sec:proposed-concept}).

Unfortunately, this project cannot cover every aspect and is largely guided by the goals set for the Telekom Challenge.
For this reason, some \textit{prerequisites and limitations}~(\ref{sec:prelimit}) have been specified.
Additionally, the data and the given training environment are explained.

In the next step the general structure and the main metrics are displayed for the \textit{experiments}~(\ref{sec:experiments}).
Also, the different models and their general structure is explained and their results are shown.

These results are then used to compare and \textit{discuss}~(\ref{sec:discussion}) the models and their results.
Possible reason for the showing are then presented.

Furthermore, the most important findings, limitations and possible future extensions are stated in a final drawn \textit{conclusion}~(\ref{sec:conclusions}).


\newpage
\subsection{DNS}

Domain Name System~(DNS) is a crucial part of how modern IT networks operate.
DNS can be compared to a classic telephone directory.
To call a person with only their name known (domain name), the name can be looked up to find the corresponding phone number (IP address) if they are registered.
In case a letter (mail) is preferred; the postal address (MX~record) can also be retrieved from the telephone directory.
Part of DNS' power lies in its recursive nature.
If a DNS resolver can not answer a query, it can query the next upstream DNS resolver and so on, until the root DNS resolver for a top-level domain~(TLD) is reached.
Everyone uses DNS several times a day, e.g. by entering the domain of a website in the address bar of the browser or by simply using a smartphone.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{introduction/how_dns_works_en.png}
    \caption[How DNS works]{How DNS works\protect\footnotemark}
    \label{fig:how-dns}
\end{figure}
\footnotetext{Source: \url{https://cdn.adguard.com/public/Adguard/kb/DNS_filtering/how_dns_works_en.png} (visited on 09/21/2021)}


\newpage
\subsection{Natural Language Processing}\label{sec:nlp}

Natural Language Processing~(NLP) is a subfield of linguistics and artificial intelligence~(AI).
It provides computers the ability to understand human language, text as well as speech.
The processing of natural language is done through machine learning and deep learning techniques.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{introduction/chat-bot.png}
    \caption[Human-Machine Interaction]{Human-Machine Interaction\protect\footnotemark}
    \label{fig:human-machine-interaction}
\end{figure}
\footnotetext{Source: \url{https://miro.medium.com/proxy/1*5bxYB2E97da3GO5Zs_A9nw.png} (visited on 09/21/2021)}

Natural Language Processing is divided in two main components:
\begin{itemize}
    \item Natural Language Understanding~(NLU)
    \item Natural Language Generation~(NLG)
\end{itemize}

\subsubsection{Natural Language Understanding}\label{sec:nlu}

Natural Language Understanding~(NLU) uses syntactic and semantic analysis of text to be able to understand the given sentence.
Semantic is used to determine the meaning of the sentence or word and syntactic is the grammatical structure of the sentence.
Since, the same words can have different meanings in different sentences, the context has to be taken into account to get a better understanding of meaning of the sentence.
This words are called homonyms words that are spelled and pronounced the same but have different meaning.

For example nail and racket are homonyms words.

\begin{minipage}[t]{.5\textwidth}
    \begin{itemize}
        \item She broke a \textit{nail} on her finger.
        \item She hammers a \textit{nail} into the wall.
        \item She \textit{nailed} all her exams.
    \end{itemize}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
    \begin{itemize}
        \item He bought a new tennis \textit{racket}.
        \item He fires a \textit{racket} in his game.
    \end{itemize}
\end{minipage}

Besides homonyms, on speech recognition homophone are also a problem.
Although these are written differently their pronunciation is the same.

\begin{minipage}[t]{.13\textwidth}
    \begin{itemize}
        \item to
        \item too
        \item two
    \end{itemize}
\end{minipage}%
\begin{minipage}[t]{.37\textwidth}
    \begin{itemize}
        \item He take the \textit{fourth} place.
        \item He is breaking \textit{forth}.
    \end{itemize}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
    \begin{itemize}
        \item The \textit{flower} on your table is beautiful.
        \item \textit{Flour} is used to make bread.
    \end{itemize}
\end{minipage}

On the other hand, homographs, words with the same spelling but different pronunciation, are a problem for text recognition.

This examples demonstrate some of the difficulties of NLU, where the machine tries to understand the given natural language.

NLU is not limited to these aspects.
Sentiment analysis would be another example.
Sentiment analysis can be used to classify comments on a blog or about a product, for example, to determine whether they are positive or negative.

\subsubsection{Natural Language Generation}

Natural Language Generation~(NLG) is also a subset of NLP and focuses on the generation of new text.
A common task on this subfield is questioning and answering, where the machine tries to answer a given question.
Another common task is text summarization.
Here, a usually long text is given and a short text will be generated, which is supposed to represent a summary of the long text.
Natural Language Generation includes three subtasks.
The first is \verb|text planning|, in which the relevant content is formulated and organized.
The second task is \verb|sentence planning|, which involves selecting the necessary words, considering the flow of the text, and creating meaningful sentences in terms of content.
The last task is \verb|text realization|, in which the planning from the previous step is used to form a grammatically correct sentence with punctuation and the correct tenses.

\begin{figure}[H]
    \centering
    \includesvg[width=\textwidth]{introduction/nlg.drawio.svg}
    \caption{Text planning, Sentence planning, Text realization}
    \label{fig:nlg-overview}
\end{figure}
