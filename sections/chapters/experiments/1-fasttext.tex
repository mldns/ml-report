\subsection{FastText}\label{sec:fasttext}

The fastText library~\cite{bojanowski2016enriching}~\cite{joulin2016bag}~\cite{joulin2016fasttext} developed by Facebook's AI Research~(FAIR) lab allows users to learn text representations and text classifiers, which shows similar results to deep neural networks.
Internally, the fastText library creates vector representations for each word while training a model.

A big difference in comparison to other neural networks is that fastText trains based on time.
This means that not the number of epochs, the validation score or any other factor determines when a model has completed its training, but a fix time which could be set in the beginning.
Facebook has set this time to $5$ minutes by default.
This makes fastText extremely fast compared to other competitors.

Furthermore, it is designed to work on generic hardware or even mobile devices.
While other deep neural networks spend most of their time training on GPUs, fastText is optimized for training on CPUs, which theoretically enables continuous development of AI models on the fly on almost any device.

\begin{figure}[H]
    \centering
    \includestandalone[width=0.55\textwidth]{fasttext/model}
    \caption{fastText architecture}
    \label{fig:fasttext-model}
\end{figure}

The architecture uses $N$ ngram features $x_1, x_2, \ldots, x_N$ as input.
These $n$-gram features are used in the hidden layer.
First a weight matrix $A$ is used as a look-up table over the words.
These representations are then used in the classifier which uses the weight matrix $B$.
Afterwards, it goes through a \textit{SoftMax} activation function $f$ to determine the corresponding class.

Internally, the fastText model minimizes the following negative log likelihood:
\begin{equation}\label{eq:ft:neg-log-likelihood}
    -\frac{1}{N} \sum_{n=1}^{N}{y_n log(f(B Ax_n))}
\end{equation}
{
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{@{\hspace*{5.75mm}\textbullet~~}l@{~~$-$~~}l}
        $x_n$ & normalized bag of features of the $n^{th}$ document \\
        $y_n$ & the label of the $n^{th}$ document                  \\
        $A$   & represents the hidden layer (weight matrix)         \\
        $B$   & the output layer                                    \\
        $f$   & the SoftMax activation function
    \end{tabular}
}

This approach is very similar to the Word2Vec model~\cite{mikolov2013efficient} which is also a two layer network with inputs of $n$-grams.
The big difference between both models are these $n$-grams, because fastText uses \textit{$n$-grams of characters} for embedding instead of words.

\begin{figure}[H]
  \centering
  \includestandalone[width=0.55\textwidth]{fasttext/3-grams}
  \caption{Example of a $3$-grams embedding}
  \label{fig:fasttext-3grams}
\end{figure}

The advantage of using only parts of a word for the embedding process is that also unknown words can be transformed.
This means that words that are not known at training time are also assigned an appropriate vector representation, instead of assigning the same representation, e.g. a null vector, to all unknown words.

Another specialty of fastText is the usage of the hashing trick~\cite{Weinbergeretal09}.
The hashing trick hashes the $n$-grams to achieve a fast and efficient lookup.

To speed up the prediction a hierarchical SoftMax~\cite{HSoftmax} in combination with the Huffmann coding tree~\cite{mikolov2013efficient} is used.
The Huffmann coding creates a multi-layer binary tree where each class is represented by a leaf.
Due to the nature of binary trees, the prediction can be done in $O(hlog_2(k))$ where $h$ is the dimension of the text representation and $k$ is the number of available classes.


\subsubsection{Training Environment}

While training, the FastText models used different parameters under various circumstances:
\begin{itemize}
    \item dimensionality of the word vectors (\verb|dim|)
    \item number of items of the contiguous sequence (\verb|wordNgrams)|
    \item metric to tune (\verb|autotuneMetric|)
    \item selection of the data (\verb|data_selection|)
    \item data preprocessing steps (\verb|data_preprocessing|)
\end{itemize}

\paragraph{Word Vector Dimensionality}
~\\
FastText uses a word vector dimension of $100$ by default.
The size of the dimensions directly affects the size of the trained model.
Hence, to get a small and efficient model, the dimensionality should be kept low.
Since, the model only needs to distinguish whether a domain is malicious or not (binary classification), a perfectly trained model would ideally only require two dimensions.

However, since this case is very unlikely, we set the dimensionality for most tests to $5$ by default.
It should be noted that tests with higher dimensionality were also performed.

\paragraph{Word {\boldmath $N$}-grams}
~\\
In order to experiment with the embeddings, we also tested different $N$-grams.
By default, fastText tends to keep the $N$-grams between $2$ and $5$.
As described earlier, these $N$-grams of words will be then embedded as $N$-grams of characters.
In this way, even unfamiliar words are given an appropriate vector representation.

\paragraph{Autotune Metric}\label{sec:autotune-metric}
~\\
FastText comes with an autotune feature which should find the best hyperparameters automatically.
Using the autotune metric, fastText aims to automatically select the best hyperparameters for a chosen metric.

The metric can be specified as follows:

\verb/[precisionAtRecall|recallAtPrecision][:<percent>[:label]]/

Thus, fastText with \verb|precisionAtRecall:98:__label__top| would aim for the best precision at a recall of $98\%$ for the label \verb|top|, i.e. for benign domains.

\newpage
\paragraph{Data Selection}
~\\
As always, models are only as good as the data they are based on.
In order to achieve an optimal test of the semi-optimal data situation, the following data combinations were tested independently to the cleaning steps mentioned in chapter \ref{sec:prelimit} \hyperref[sec:prelimit]{Prerequisites \& Limitations}:
\begin{itemize}
    \item \verb|whitelist_blacklist|
    \item \verb|whitelist_blacklist_unbalanced|
    \item \verb|whitelist_blacklist_cutted|
    \item \verb|whitelist_blacklist_and_dga|
    \item \verb|whitelist_blacklist_and_dga_cutted|
\end{itemize}

Note that all of the defined data selection methods always remove all domains that do not have a valid top level domain.

\subparagraph{\texttt{whitelist\_blacklist}}
is based on the whitelist and the blacklist.
A special feature is that the larger list is shrunk to the size of the smaller one.
Thus, we get a balanced dataset where the number of domains from the whitelist and blacklist are always identical.

\subparagraph{\texttt{whitelist\_blacklist\_unbalanced}}
is, like \verb|whitelist_blacklist|, also based on the whitelist and the blacklist.
In this case, no adjustments are made.
As a result, the following training is performed with an unbalanced dataset.

\subparagraph{\texttt{whitelist\_blacklist\_cutted}}\label{sec:ft-whitelist-blacklist-cutted}
uses only the top $N$ domains in addition to the blacklist.
As known, our whitelist is unfortunately not a verified whitelist, but the list of the top $1$ million accessed domains.
Therefore, the probability that a domain on this list is malicious increases with each additional position.
Thus, we assume that as the popularity of a domain decreases, the probability of a malicious domain increases.
By default $N$ is set to $100,000$.
As for \verb|whitelist_blacklist|, the larger list is shrunk to the size of the smaller one to avoid unbalanced dataset.

\subparagraph{\texttt{whitelist\_blacklist\_and\_dga}}
is similar to \verb|whitelist_blacklist_unbalanced|.
The big difference is that the blacklist is extended by the list of \textit{domain generator algorithm}~(DGA) domains.
Please note that the resulting dataset is unbalanced.

\subparagraph{\texttt{whitelist\_blacklist\_and\_dga\_cutted}}
is like \verb|whitelist_blacklist_and_dga|.
The only difference is that the whitelist is additionally restricted to $N$ domains.
The reason for this is again the probability of a malicious domain.
Again, the default for $N$ is $100,000$.
Naturally, the resulting dataset is also unbalanced.

\paragraph{Data Preprocessing}
~\\
In order to further improve performance, a wide range of data preprocessing options are offered, which can be combined as desired during training:
\begin{itemize}
    \item \verb|lower_case_split_by_dot_dash_underscore|
    \item \verb|remove_tld|
    \item \verb|trim_leading_www|
    \item \verb|domain_level_N|
    \item \verb|max_length_N|
    \item \verb|max_word_length_N|
\end{itemize}

\subparagraph{\texttt{lower\_case\_split\_by\_dot\_dash\_underscore}}\label{sec:lower-case-split-by-dot-dash-underscore} $-$~~
As described in \hyperref[sec:prelimit]{Prerequisites \& Limitations} (\ref{sec:prelimit}), the most important preprocessing step is the generation of \textit{domain sentences}.
To accomplish this dots, underscores and dashes will be each replaced with a single space character.
This preprocessing step is the default and is always applied.

Example:~~~\verb|www.google.de|~~~{\boldmath $\rightarrow$}~~~\verb|www google de|

\subparagraph{\texttt{trim\_leading\_www}}\label{sec:trim-leading-www} $-$~~
Many domains use the subdomain \verb|www|.
Even if the usage brought many advantages and conveniences in the past, this provides no additional benefit with respect to our classification task.
For this reason, it might be a good idea to remove a leading \verb|www| sequence.

Example:~~~\verb|www.google.de|~~~{\boldmath $\rightarrow$}~~~\verb|google.de|

\subparagraph{\texttt{remove\_tld}}\label{sec:remove-tld} $-$~~
Top level domains~(TLD) such as $.de~.com~\ldots$ say only indirectly something about the actual domain.
Many large companies also do not offer their sites under only one domain.
Google is a good example, hence their services are available under \verb|google.de|, \verb|google.com| and many more.
However, if you want both domains to be treated equally, a possible solution would be to remove all TLDs and only evaluate domains only based on their \textit{second level domain}~(SLD) and subdomains.

Example:~~~\verb|google.de|~~~{\boldmath $\rightarrow$}~~~\verb|google|

\subparagraph{\texttt{domain\_level\_N}}\label{sec:domain-level-N} $-$~~
Some domains come with many subdomains.
However, the most critical information lies in the SLD and maybe the first subdomain.
With \verb|domain_level_2| and \verb|domain_level_3| you can focus on these areas.

Example:~~~\verb|sub3.sub2.sub1.abc.xx|~~~{\boldmath $\rightarrow$}~~~\verb|abc.xx|\\
Example:~~~\verb|sub3.sub2.sub1.abc.xx|~~~{\boldmath $\rightarrow$}~~~\verb|sub1.abc.xx|

\subparagraph{\texttt{max\_length\_N}}\label{sec:max-length-N} $-$~~
Some domains are really long.
If you want to concentrate only on the $N$ most important characters, you can use preprocessing methods like \verb|max_length_N|.
Predefined are \verb|max_length_30|, \verb|max_length_40|, \verb|max_length_50| and \verb|max_length_100|.

Example $N=30$:~~~\verb|abc[...]xyzabc[...]xyz|~~~{\boldmath $\rightarrow$}~~~\verb|wxyzabc[...]xyz|

\subparagraph{\texttt{max\_word\_length\_N}}\label{sec:max-word-length-N} $-$~~
Some subdomains tend to be extremely long.
If this is to be prevented, one possibility is to split each word after $N$ characters.
To achieve this \verb|max_word_length_1|, \verb|max_word_length_3| and \verb|max_word_length_5| are available.

Example $N=1$:~~~\verb|sub3 sub2 sub1 abcde xx|~~~{\boldmath $\rightarrow$}~~~\verb|s u b 3 s u b 2 s u b 1 a b c d e x x|\\
Example $N=3$:~~~\verb|sub3 sub2 sub1 abcde xx|~~~{\boldmath $\rightarrow$}~~~\verb|sub 3 sub 2 sub 1 abc de xx|


\subsubsection{Trainings Setup}\label{sec:ft-training-setup}

A total of $150$ runs of different model configurations with different data and data preprocessing steps were performed.
Out of these $150$ runs, $137$ have been successfully completed, the rest have failed.
If a single test failed, it was repeated up to four additional times until the particular test specification was determined to be infeasible.
The error message of these tests was \verb|Segmentation fault (core dumped)|, which can occur while training a model with fastText.
The community does not reach a mutual opinion, but some suspected that there is not enough memory available to train the model in this cases, while other reach the conclusion that this could be a bug which appears if the learning rate is to large~\cite{FT-CoreDump-lr}.
There are several such discussions~\cite{FT-CoreDump-issues} about exactly this error with different opinions.
However, it is observable that for some instances this error appears randomly, even if their conditions are the same.

To maintain representative results, all test runs were performed under three seeds~$(42,~365,~4711)$.
Therefore, the training procedure can be captured very roughly with the following Nassi-Shneiderman diagram:
\begin{figure}[H]
    \centering
    \includesvg[width=0.4\textwidth]{fasttext/ft-training-procedure.drawio.svg}
    \caption{FastText: Training procedure}
    \label{fig:ft-trainings-procedure}
\end{figure}

As the figure \ref{fig:ft-trainings-procedure} illustrates, all tests are not only performed with one whitelist.
In order to cover a wide range not only the cleaned whitelist, but also with the whitelist before the cleaning steps from section \ref{sec:whitelist} are investigated.

\verb|Tests|, in this case, covers a combination of the various \textit{data preprocessing} methods and manually set fastText hyperparameters:
\begin{itemize}
    \item \verb|5D|
    \item \verb|5D + www-cut + without-tld|
    \item \verb|20D + www-cut + without-tld + word-length-5|
    \item \verb|5D + ngrams-3 + www-cut + without-tld + word-length-3|
    \item \verb|10D + autotuneMetric-r2p-96-top|
\end{itemize}

\paragraph{\texttt{5D}}
prescribe almost nothing to fastText and only sets the dimension of the word vectors to $5$.
Furthermore, no separate preprocessing methods are used.

\paragraph{\texttt{5D + www-cut + without-tld}}
similar to \verb|5D|, the dimension of the word vectors is set to $5$.
However, \hyperref[sec:trim-leading-www]{\texttt{trim\_leading\_www}} and \hyperref[sec:remove-tld]{\texttt{remove\_tld}} are used as preprocessing methods.

Thus, for example, \verb|www.google.de| becomes simply \verb|google|.

\paragraph{\texttt{20D + www-cut + without-tld + word-length-5}}
is nearly equivalent to \texttt{5D + www-cut + without-tld}.
The difference is that the dimensionality is set to $20$ and an additional preprocessing method \texttt{word-length-5} is used.
In short, the methods \hyperref[sec:trim-leading-www]{\texttt{trim\_leading\_www}}, \hyperref[sec:remove-tld]{\texttt{remove\_tld}} and \hyperref[sec:max-word-length-N]{\texttt{max\_word\_length\_5}} are applied here.

Thus, for example, \verb|www.google.de| would result in \verb|googl e|.

\paragraph{\texttt{5D + ngrams-3 + www-cut + without-tld + word-length-3}}
does not only sets the dimension of the word vectors to $5$ but also the ngrams fix to $3$.
Almost identical to the previous test the preprocessing methods are \hyperref[sec:trim-leading-www]{\texttt{trim\_leading\_www}}, \hyperref[sec:remove-tld]{\texttt{remove\_tld}} and \hyperref[sec:max-word-length-N]{\texttt{max\_word\_length\_3}}.

\paragraph{\texttt{10D + autotuneMetric-r2p-96-top}}
does not set any special preprocessing methods.
However, the test not only sets the word vector dimension to $10$, but also uses the \hyperref[sec:autotune-metric]{autotune metric} with the specification \verb|recallAtPrecision:96:__label__top|.
This means that fastText aims to get the best recall at a precision of $96\%$ for not malicious domains.


\subsubsection{PCA}

All FastText tests also trains a \textit{Principal Component Analysis}~(PCA) which is done by using the word vectors of the fastText model.
In the initial tests, PCA models usually lead to very good results.
For this reason, a PCA model is automatically trained by default.
Later, however, this trend could no longer be observed.
On the contrary, during the last test runs, it could be consistently observed that the PCA models were getting worse results in comparison to the fastText models.
Therefore, PCA will not be further discussed in this project.
Nevertheless, all relevant data can be found in the \hyperref[apx:pca]{appendix}.


\newpage
\subsubsection{Results}\label{sec:ft-results}

In the following table~\ref{table:fasttext-metrics-10} the metrics of the top $10$ FastText models are displayed.
The full table can be found in the \hyperref[apx:ft:metrics]{appendix}.
Both tables are sorted by the \textit{accuracy} followed by \textit{bad recall}, \textit{top recall} and the \textit{model size} of the models on the test dataset.

\begin{table}[H]
    \centering
    \nprounddigits{5}
    \csvreader[%
        head to column names,
        centered tabular=llllllr,
        table head=~ & ~ & \bfseries Bad & \bfseries Bad & \bfseries Top & \bfseries Top & \bfseries Model Size\\
        \bfseries Run ID & \bfseries Accuracy & \bfseries Precision & \bfseries Recall & \bfseries Precision & \bfseries Recall & \bfseries in MB\\\toprule,
        late after line=\\, late after last line=\\\bottomrule,
        respect underscore=true,
        filter={\value{csvrow}<10},
        preprocessor={\csvencode},
    ]{data/experiments/fasttext.csv}{
        run_id=\runid,
        metrics.accuracy=\accuracy,
        metrics.bad_precision=\badprecision,
        metrics.bad_recall=\badrecall,
        metrics.top_precision=\topprecision,
        metrics.top_recall=\toprecall,
        metrics.model_size_in_mb=\modelsize,
    }{%
        \small\texttt{\StrMid{\runid}{0}{7}} &
        $\np{\accuracy}$ &
        $\np{\badprecision}$ &
        $\np{\badrecall}$ &
        $\np{\topprecision}$ &
        $\np{\toprecall}$ &
        \nprounddigits{3}$\np{\modelsize}$
    }
    \caption{FastText: Metrics - Top $10$}\label{table:fasttext-metrics-10}
\end{table}

All top $10$ models have a similar accuracy score over $0.95$ where the best model have an accuracy of $0.96023$.
It can be clearly seen that the model size differ a lot and no direct correlation can be seen between accuracy and the model size.
The precision scores are all above $0.94$ with the \textit{top} precision being better than the \textit{bad} precision scores with even above $0.96$.
Respectively, the corresponding recall for \textit{top} and \textit{bad} are overall above $0.946$ with bad recall even above $0.96$.

\begin{table}[H]
    \centering
    \nprounddigits{5}
    \csvreader[%
        head to column names,
        centered tabular=lrrrrl,
        table head=~ & ~ & ~ & ~ & ~ &  manual set\\
        \bfseries Run~ID & \bfseries Epochs & \bfseries Dimensionality & \bfseries Seed & \bfseries Word {\boldmath $N$}-grams & \bfseries Training Parameters\\\toprule,
        %table foot=\caption{Parameters},
        late after line=\\, late after last line=\\\bottomrule,
        respect underscore=true,
        filter={\value{csvrow}<10},
        %preprocessor={\csvsort[-k5rn -k7rn -k11rn -k9n]},  % accuracy, badrecall, toprecall, modelsize
        preprocessor={\csvencode},
    ]{data/experiments/fasttext.csv}{
        run_id=\runid,
        params.epoch=\epoch,
        params.dim=\dim,
        params.seed=\seed,
        params.wordNgrams=\wordNgrams,
        params.manual set training params=\manualsettrainingparams,
    }{%
        \small\texttt{\StrMid{\runid}{0}{7}} &
        $\epoch$ &
        $\dim$ &
        $\seed$ &
        $\wordNgrams$ &
        \small\manualsettrainingparams
    }
    \caption{FastText: Parameters - Top $10$}
    \label{table:fasttext-param-10}
\end{table}

In table~\ref{table:fasttext-param-10} the parameters for the top $10$ models can be seen.
The full table is available in the \hyperref[apx:ft:parameters]{appendix}.

FastText has a built-in hyperparameter optimization which chooses parameter on its own.
Along the seed, the parameters we set manually can be seen in the column \textit{manual set Training Parameters}.
Hereby, \texttt{dim} represents the dimensionality, the size of the word vectors, and \texttt{autotuneMetric} the specific metric to tune.
If the \texttt{autotuneMetric} is manually set, \verb|recallAtPrecision:96:__label__top| was used.
The number of epochs of the top $10$ models are most of the time $100$ or $58$.
Also, all three seeds which were tested $(42, 365, 4711)$ are on the top $10$ list.
This suggests that these results are not due to coincidence, but reliable.
Furthermore, $5$-grams are the most common on the table with only one exception, run id \verb|048bca0|.
The dimensionality shown in the table is either $5$ or $10$, although even models with higher dimensions were trained.
Therefore, it can be concluded that in the given scenario, a higher dimension does not have much impact on the performance.

\begin{figure}[H]
	\hspace*{\fill}
    \subfloat[Top $10$\label{fig:ft-model-sizes:a}]{%
        \includesvg[width=.7\textwidth]{plots/fasttext/model_size_top_10_in_mb.svg}
    }
    \hspace*{\fill}\hspace*{\fill}
    \subfloat[Boxplot over all runs\label{fig:ft-model-sizes:b}]{%
        \includesvg[width=.26\textwidth]{plots/fasttext/model_size_in_mb_boxplot.svg}
        \vspace*{4mm}
    }
    \hspace*{\fill}
	\caption{FastText: Model size in MB}
	\label{fig:ft-model-sizes}
\end{figure}

Figure~\ref{fig:ft-model-sizes} shows the model size in megabytes~(MB).
On the left side, the top $10$ models in subfigure~(\hyperref[fig:ft-model-sizes:a]{\texttt{a}}) are shown as a bar chart.
On the right side, in subfigure~(\hyperref[fig:ft-model-sizes:b]{\texttt{b}}), a boxplot over all experiment runs performed can be found.

In subfigure~(\hyperref[fig:ft-model-sizes:a]{\texttt{a}}) it can be seen that no direct correlation between the performance and the model size is noticeable.
Many similar model sizes occur distributed and some very different ones occur directly next to each other.

In subfigure~(\hyperref[fig:ft-model-sizes:b]{\texttt{b}}), the boxplot reveals that the median model size is about $195$~MB.
In general, the models tend to be between $20$~MB and $280$~MB.

However, it is interesting to note that two of the models in the top $10$ are outliers and have an increased model size.

\begin{figure}[H]
	\centering
    \subfloat[Top $10$\label{table:fasttext-top-metrics:a}]{%
        \includesvg[width=\textwidth]{plots/fasttext/metrics_top_10.svg}
    }
    \phantomcaption
\end{figure}\vspace*{-8mm}
\begin{figure}[H]
    \ContinuedFloat
    \centering
    \subfloat[Boxplot over all runs\label{table:fasttext-top-metrics:b}]{%
        \includesvg[width=.8\textwidth]{plots/fasttext/metrics_boxplot.svg}
    }
	\caption{FastText: Metrics}
	\label{fig:ft-metrics}
\end{figure}

Figure~\ref{fig:ft-metrics} shows in the upper area~(\hyperref[table:fasttext-top-metrics:a]{\texttt{a}}) a visual representation of table~\ref{table:fasttext-metrics-10} and in the lower area~(\hyperref[table:fasttext-top-metrics:b]{\texttt{b}}) a boxplot for each metric over all runs.

Subfigure~(\hyperref[table:fasttext-top-metrics:a]{\texttt{a}}) visualizes very well that the \textit{bad precision} and \textit{top recall}, as well as \textit{top precision} and \textit{bad recall} are related.
In addition, it is easy to see that the former always performs slightly worse and the latter always slightly better than the \textit{accuracy}.

The subfigure~(\hyperref[table:fasttext-top-metrics:a]{\texttt{b}}) shows that all metrics have a median of approximately $0.9$ or higher.
The \textit{accuracy} metric is the only metric that has an outlier.
However, this outlier is in the immediate vicinity of the actual boxplot and thus does not deviate very much.
Furthermore, it is easy to see that the variance is lowest for \textit{accuracy}.

The relationship between \textit{bad precision} and \textit{top recall} as well as \textit{bad recall} and \textit{top precision} can also be seen here.
Respectively, the two boxplots are each very similar to each other.
However, \textit{bad precision} and \textit{top recall} show a much greater dispersion than \textit{bad recall} and \textit{top precision}.

%Subfigure~(\hyperref[table:fasttext-top-metrics:a]{\texttt{b}}) also shows that the first quartile is mostly above $0.8$ in two cases even close to $0.9$.
%On average, the first quartile is around $0.85$.

\begin{table}[H]
    \centering
    \nprounddigits{5}
    \csvreader[%
        head to column names,
        centered tabular=llllllr,
        table head=~ & ~ & \bfseries Bad & \bfseries Bad & \bfseries Top & \bfseries Top & \bfseries Model Size\\
        \bfseries Run ID & \bfseries Accuracy & \bfseries Precision & \bfseries Recall & \bfseries Precision & \bfseries Recall & \bfseries in MB\\\toprule,
        late after line=\\, late after last line=\\\bottomrule,
        respect underscore=true,
        filter={\value{csvrow}<10},
        preprocessor={\csvencode},
    ]{tables/fasttext/ft_top_metrics.csv}{
        run_id=\runid,
        metrics.accuracy=\accuracy,
        metrics.bad_precision=\badprecision,
        metrics.bad_recall=\badrecall,
        metrics.top_precision=\topprecision,
        metrics.top_recall=\toprecall,
        metrics.model_size_in_mb=\modelsize,
    }{%
        \small\texttt{\StrMid{\runid}{0}{7}} &
        $\accuracy$ &
        $\badprecision$ &
        $\badrecall$ &
        $\topprecision$ &
        $\toprecall$ &
        \nprounddigits{3}$\modelsize$
    }
    \caption{FastText: Top Metrics}\label{table:fasttext-top-metrics}
\end{table}

Table~(\ref{table:fasttext-top-metrics}) shows the best models for each metric.
Thus, as before, each line represents one model.
The highlighted metrics are the best achieved values in the whole experiment.

In this context, a good metric in one area cannot be equated with other areas.
For example, in the first line the \textit{accuracy} is excellent, but the \textit{model size} is quite large.

For this reason, it should never only one metric be decisive, but always a combination of the relevant ones in the given situation.

Nevertheless, you can see from this table that besides the highlighted metric, the other metrics also perform relatively well.


\begin{table}[H]
    \centering
    \nprounddigits{5}
    \csvreader[%
        head to column names,
        centered tabular=lllc,
        table head=\bfseries Run ID & \bfseries Data Selection & \bfseries Whitelist & \bfseries Data Preprocessing\\\toprule,
        late after line=\\, late after last line=\\\bottomrule,
        respect underscore=true,
        filter={\value{csvrow}<10},
        preprocessor={\csvencode},
    ]{data/ft_data_source.csv}{
        run_id=\runid,
        data_selection=\dataselection,
        whitelist_path=\whitelistpath,
        data_preprocessing=\datapreprocessing,
    }{%
        \small\texttt{\StrMid{\runid}{0}{7}} &
        \small\texttt{\dataselection} &
        \StrBehind[1]{\whitelistpath}{./datasets/}[\mystring]\small\texttt{\StrBefore[1]{\mystring}{.txt}} &
        \StrSubstitute{\datapreprocessing}{, }{,}[\mystring]%
        \noindent\foreach \letter in \mystring
        {%
            \circled{\texttt{\letter}}\space%
        }
    }
    \caption{FastText: Data Sources - Top $10$}\label{table:fasttext-top-data-sources}
\end{table}

Table~(\ref{table:fasttext-top-data-sources}) lists which data and preprocessing steps were made for each top $10$ run.
As always, the complete table can be looked up in the \hyperref[apx:ft:data-sources]{appendix}.

It is striking that all top $10$ runs use the same data selection \verb|whitelist_blacklist_cutted|~(\ref{sec:ft-whitelist-blacklist-cutted}).
This means that all whitelist and blacklist entities were cutted to a total of $100,000$ domains.

Also, the data preprocessing step is the same for each run.
\circled{\texttt{A}} symbolizes the default preprocessing step \verb|lower_case_split_by_dot_dash_underscore|~(\ref{sec:lower-case-split-by-dot-dash-underscore}) where the dots, dashes and underscores where replaced with a space character.
No further preprocessing steps were applied.

It is noticeable that in the top $6$ runs the whitelist~(\ref{sec:whitelist}) was used without further cleaning steps.
Only the next runs used the cleaned whitelist~(\ref{sec:whitelist}), from which many invalid or duplicate domains were removed from the list.
