\subsection{TensorFlow}\label{sec:tensorflow}

In addition to \hyperref[sec:fasttext]{FastText}, TensorFlow~\cite{Tensorflow2015-whitepaper}, an open-source machine learning platform, was explored for use in the project and several models were trained.
Since FastText has a fixed structure, TensorFlow models were investigated, as these offers the flexibility to test different architectures with
different parameters.
From the ten TensorFlow architectures examined, the following five models are of notable interest:
\begin{itemize}
    \item \hyperref[sec:tf-basic]{Basic}
    \item \hyperref[sec:tf-bi]{Bidirectional}
    \item \hyperref[sec:tf-gru]{GRU}
    \item \hyperref[sec:tf-distilbert]{DistilBERT}
    \item \hyperref[sec:tf-ft]{FastText replicated}
\end{itemize}

The initial structure of all tested models uses a tokenizer and an embedding layer.
A tokenizer vectorizes a text, which means that it converts the given text into a sequence of integers.
The embedding layer transforms the given sequence into a floating vector, where it tries to find similar vectors for similar sentences or texts.

These steps are important since machine learning algorithms can not use characters as inputs but expect numbers.
Also, the embedding step helps the model with the classification task, as similar sentences will create similar floating vectors.

Furthermore, a wrapper called ktrain~\cite{maiya2020ktrain} was used to train different models, which especially helps when using Transformers from Huggingface~\cite{wolf-etal-2020-transformers} in TensorFlow.
\begin{shadequote}[r]{\href{https://github.com/amaiya/ktrain}{ktrain}}
ktrain is a lightweight wrapper for the deep learning library TensorFlow Keras (and other libraries) to help build, train, and deploy neural networks and other machine learning models.
\end{shadequote}

The following models were trained with help of ktrain:
\begin{itemize}
    \item DistilBERT
    \item BiGRU
    \item GRU
    \item FastText
\end{itemize}

It should be noted that the training history plots of the top $10$ TensorFlow models can be found in the \hyperref[apx:ft:data-sources]{appendix}.


\subsubsection{Training Setup}\label{sec:tf-training-setup}

The training procedure can be described with the following Nassi-Shneiderman diagram:
\begin{figure}[H]
    \centering
    \includesvg[width=0.4\textwidth]{tensorflow/tf-training-procedure.drawio.svg}
    \caption{TensorFlow: Training procedure}
    \label{fig:tf-trainings-procedure}
\end{figure}

The training setup on TensorFlow is very similar to the training setup of FastText~\ref{sec:ft-training-setup}.


\subsubsection{Basic}\label{sec:tf-basic}

The first model was a simple neural network with the initial structure (as explained above) and with additional dropout layers, one \verb|GlobalAveragePooling| layer and a dense output layer.

The \verb|Dropout| Layer randomly sets input units to $0$ and helps to prevent overfitting, which means the network does not rely on one single path through the network.
Dropout layers are only active during the training phase.

The \verb|GlobalAveragePooling| layer was used because it outputs a fixed length vector by averaging over the sequence, which helps with variable input length.

Next, the output was piped through a fully-connected (\verb|Dense|) layer with one hidden neuron.
It has only one neuron as the problem to be solved is a binary classification task and the model only needs to decide if it is one or the other class, benign or malicious.

This model serves as a baseline for later comparisons.


\subsubsection{Bidirectional}\label{sec:tf-bi}

Bidirectional layers give the neuronal network the ability to read the sentence forwards and backwards.
This can be done with two recurrent neural networks (RNN).

Recurrent neural network (RNN) have a \textit{feedback loop} which is also implied by the name \enquote{recurrent}.

Many neural networks have only a \textit{feed-forward} activation which occurs from the input layer to the output layer.
Recurrent neural networks, on the other hand, also use a feedback connection, i.e. a connection that points to itself.
Since these kind of neural networks have a feedback loop, they contain a \textit{memory}.

This kind of neural networks offers the advantage to yield good results on classifying sequences, like text.
The \textit{memory} part is achieved through an input from a previous time step.
More specifically, the corresponding output is used in the current time step.
Therefore, the previous time step has an influence on the current prediction.
Due to this reason, RNNs are well suitable for sequences.
The bidirectional layer uses, for example, two Long Short-Term Memory's~(LSTM)~\cite{LSTM}.

These RNNs have the specialty to offer additional gates and a so-called long term memory.
The LSTM has two inputs, one of them being the short term memory from the previous output and the second being the long term memory.
Additionally, gates filter what should be kept, updated or forgotten from this time step.

There are three gates in a LSTM cell:
\begin{itemize}
    \item \textit{forget gate} that controls the parts of the long-term memory which should be forgotten
    \item \textit{input gate} that controls the elements which should be added to the long-term memory
    \item \textit{output gate} that controls what is used as the output and therefore what is the next short term memory
\end{itemize}

Consequently, a bidirectional layer uses two LSTM layers, with the input sequence passing normally, i.e., \textit{forward}, through one of the LSTMs.
The second input is the reverse sequence, i.e. from the end to the beginning.
This sequence then runs through the second LSTM.

Afterwards, the output of both layer will be concatenated.
This neural network architecture has the advantage to read both ways and therefore using both information, from the \textit{past} and from the \textit{future}.
Hence, it could have a better understanding of context in a sequence.


\subsubsection{GRU}\label{sec:tf-gru}

The Gated Recurrent Unit (GRU)~\cite{GRU} is similar to the LSTM network.
It also uses gates and a memory.
In the GRU network, the \textit{input gate} and the \textit{forget gate} were combined into an \textit{update gate}.

This \textit{update gate} works in such a way that a $1$ corresponds to an open \textit{forget gate}.
At the same time, a $1$ means that the \textit{input gate} is closed.
A $0$ corresponds to the complementary.
All values in between correspond to the partial opening or closing of these gates.
Unlike LSTM, a GRU cell has only one memory.
Another difference is that the GRU cell does not have two outputs or an \textit{output gate}.
The entire state is thus used simultaneously as output and as memory for the next time step.

\paragraph{Grure}\label{sec:tf-gru-grure}
~\\
Later on the models a \verb|grure| model is shown which means it is a normal GRU model with the additional regularization to avoid overfitting.
The regularization are the L2-Norm and some dropout layers.


\subsubsection{DistilBERT}\label{sec:tf-distilbert}

DistilBERT~\cite{DistilBERT} is a \textit{distilled} version of BERT ~\cite{devlin-etal-2019-bert}, a bidirectional encoder representations from Transformers which is lighter, faster and smaller than the regular BERT.

\paragraph{Transformer}
~\\
The architecture of this kind of neural network is called Transformer~\cite{vaswani_attention_2017}.
The Transformer neural network has three main concepts:

\begin{enumerate}
    \item The positional encodings, where each sequence is encoded with its position in the sequence.
        The difference with RNN is that RNN understands word order by processing words sequentially.
        In position encoding, the position of the word in the sentence can be seen as an additional parameter of each word.

    \item Attention (\textit{self-attention}), this can be simply explained by thinking of a translation task.
        On a translation task, e.g., from english to french, the order and the gender of words can be different.
        Attention is therefore a mechanism that allows a text model to consider every single word in the original sentence when making a decision, but with a special attention on particular words which might be the interesting part of the sentence.
        So \textit{self-attention} allows to make an association between any word in the input.

        For example, for a sentence \textit{how are you}, it can build the association from \textit{you} to \textit{are} and \textit{how}.
        This association is bundled in a score matrix where higher scores mean more \textit{attention}.
        The \texttt{SoftMax} activation function is applied afterwards, which allows for high values in the score matrix to remain large and low values to become smaller.
        So at this time step, the attention is clearer.

    \item Sequence-to-Sequence~\cite{seq2seq} learning, where a sentence is first encoded with a RNN and then decoded by another RNN.
        This is applied in machine translation.
        The same idea can be used on Transformer models where the encoding part is not a RNN, but a construct of embedding, positional encoding and attention.
        This is used as an input in the decoder part, which is very similar to the encoder part.
        One of the big advantages of this structure is that the sequence does not need to be walked through step by step, as in a normal recurrent neural network.

        This structure allows transfer learning or fine-tuning of pretrained models, which is difficult or near to impossible with RNN, due to the sequential structure.
\end{enumerate}

\paragraph{Knowledge Distillation}\label{knowledgedistil}
~\\
Knowledge distillation was introduced by Bucila et al.~\cite{modelcompression} and was then further generalized or discussed by Hinton et al.~\cite{distillHinton}.
The later, so the method by Hinton was used to create the DistilBERT model.
Knowledge Distillation is used to get smaller models with nearly the same performance as the former models.
Large models have a huge network but this network or knowledge capacity might not be fully utilized.
In addition, calculating the prediction for a large model can be very computationally intensive and therefore requires the appropriate hardware.
The knowledge distillation uses the idea of a teacher and a student.
On the on hand, the teacher is the larger model which is already trained and therefore has the \textit{knowledge}.
On the other hand, the student is a smaller model that has not yet been trained and needs to train with the help of the teacher.

More specifically, on a classification task the output of the teacher's softmax activation function can be further used to train the student model.
This output distribution is in many cases such that one class has a very high probability and the others have a probability close to zero.
The knowledge lies in the fact that the other classes only have an almost zero probability.

For example, we have the task to decide whether a ball is a volleyball or not.
If now a basketball is to predicted it still would have a higher probability than for example a class like desk.
This knowledge that both are balls is therefore forwarded to the student which don't need to learn it from scratch.
Usually, all other possible outcomes have a probability of exactly zero and thus the student has to learn everything on his own.

So instead to train on the hard truth with a cross-entropy function, it is trained on a smoothed loss of the softmax.
The equation \ref{eq:softmaxT} is the first equation in the paper of Hinton et al.~\cite{distillHinton}.
This softmax activation has a further parameter $T$ which is called temperature.
\begin{equation}\label{eq:softmaxT}
    q_i = \frac{exp(z_i/T)}{\sum{j}/T}
\end{equation}

If $T=1$ this would be a normal softmax activation function, where the probability for a specific class $q_i$ is predicted with the logit or output of the network $z_i$ compared to the other logits.
In the distillation process a larger value than $1$ is used for the temperature value to smoothing out the probabilities for the classes.
If $T \rightarrow +\inf$ it would become uniform distributed and if $T \rightarrow 0$ it would be equivalent to an one-hot target vector.

In the training process, the teacher is only used to obtain the logits.
The teacher is no longer explicitly trained.
The only model to be trained is the student model.

The loss can be therefore summarized in the following equation:
\begin{equation}\label{eq:distilLoss}
    Loss_{\alpha, T} = \alpha Loss_{soft,T} + (1 - \alpha) Loss_{hard}
\end{equation}

The usage of the smooth distribution results in the soft loss determined by the Kullback-Leibler divergence between both distributions.
\begin{equation}\label{eq:totalLossdistill}
    Loss_{soft,T} = - \sum_{i} p_i log(q_i)
\end{equation}

Let $p_i$ be the softmax distribution of the teacher and $q_i$ be the softmax distribution of the student~(\ref{eq:softmaxT}).
Both have the same temperature value $T$.
Furthermore, $Loss_{hard}$ is the cross entropy loss function of the student to the hard truth, so with a temperature score of $1$.
The parameter $\alpha$ chooses how much the normal hard or the distillation loss should be considered in this loss.

All can be summarized in the following equations:
\begin{equation}
    \begin{aligned}
        &y_i(x|T) = \frac{e^{z_i(x)}/T}{\sum_{j} e^{z_j(x)}/T} \\
        E(x|T) = \alpha (-T^2) & \sum_{i} \hat{y_i}(x|T) \log y_i(x|T) - (1-\alpha) \sum^{}_{i}\overline{y_i} \log y_i(x|1)
    \end{aligned}
\end{equation}

First, $y_i(x|T)$ is the soft, more precisely the smoothed softmax function, which corresponds to the one in equation~\ref{eq:distilLoss}.
The second equation shows the equation~\ref{eq:totalLossdistill} in more detail.

The soft loss scales with $1/T^2$.
To compensate this a scaling factor of $T^2$ is used.
Also, it can be seen that with a scaling factor of $T^2$ that the contributions of both stays the same with different temperature values~\cite{distillHinton}.

The $\hat{y}$ in the equation denotes to the output by the teacher model, $\overline{y}$ is the ground truth and $y$ is the output of the student model or the distilled model.

\paragraph{DistilBERT Architecture \& Knowledge Distillation}
~\\
The same architecture of BERT is applied to DistilBERT but without the \verb|token-type embeddings|, the \verb|pooler| layers.
Additionally, the number of layers of the student model is reduced by a factor of $2$.


\subsubsection{MobileBERT}
MobileBERT~\cite{mobilebert} uses knowledge distillation~\ref{knowledgedistil} and bottlenecks.
In general, it is as deep as BERT\cite{devlin-etal-2019-bert}, but with narrower layers.
Therefore, it is similar to~\ref{sec:tf-distilbert} with the distillation process, but uses additionally bottlenecks and instead of using half of the number of layers like in~\ref{sec:tf-distilbert}, it uses all layers but narrower.

Unfortunately, MobileBERT delivers very poor results.
Therefore, we will not go into more detail about this particular Transformer model.
All results of MobileBERT can be found in the \hyperref[apx:tensorflow:tf-klearn]{appendix}.


\subsubsection{FastText replicated}\label{sec:tf-ft}

This model is a replication of a fastText model in TensorFlow.
The intention was to achieve similar results without being limited by the restrictions of fastText.
However, the model did not work as expected.

In this FastText approach, a \verb|SpatialDropout| layer was used which drops not only individual elements but an entire 1D dimension.
This can lead to better independence of highly correlated feature maps, because strongly correlated feature maps are avoided by the dropout function.

After that, a \verb|GlobalMaxPooling| layer is placed which downsamples the input by taking the maximum value over the time dimension.
The next layer is a \verb|BatchNormalization| layer which during training normalizes the output using the mean and standard deviation of the current batch of inputs.

After that, a normal fully connected layer with a ReLU activation function, a dropout layer and a fully connected layer as output were used.


\subsubsection{BiGRU}\label{sec:bigru}

BiGRU~\cite{bigru} is short for Bidirectional GRU.
This model uses the pretrained word vector of fastText with $3$ million word vectors which was trained on $157$ languages and has an embedding size from $300$.

This model also allows a comparison to the bidirectional model mentioned earlier~(\ref{sec:tf-bi}).


\subsubsection{Results}\label{sec:tf-results}

In the following table~\ref{table:tf-ktrain-metrics-top} the metrics for top $10$ models can be seen.
The runs trained with TensorFlow are represented by a \circled{\texttt{T}} and runs trained with ktrain are represented by \circled{\texttt{K}}.
The \textit{type} column displays the different model architectures.

Overall, the best model is a finetuned distilbert model with an accuracy of $0.946$.
The only shortcoming of this model is the model size.

From the second best model onwards, the metrics decrease significantly.
However, it is notable that non distilbert models tend to be much smaller in model size.

{
    \setlength{\tabcolsep}{5.5pt}
    \begin{table}[H]
        \centering
        \nprounddigits{5}
        \csvreader[%
            head to column names,
            centered tabular=@{\space}c@{\space}lllllllr,
            table head=~ & ~ & ~ & ~ & \bfseries Bad & \bfseries Bad & \bfseries Top & \bfseries Top & \bfseries Model Size\\
            ~ & \bfseries Run ID & \bfseries Type & \bfseries Accuracy & \bfseries Precision & \bfseries Recall & \bfseries Precision & \bfseries Recall & \bfseries in MB\\\toprule,
            late after line=\\, late after last line=\\\bottomrule,
            respect underscore=true,
            filter={\value{csvrow}<10},
            preprocessor={\csvencode},
        ]{data/experiments/tensorflow_klearn.csv}{
            run_id=\runid,
            metrics.accuracy_test=\accuracy,
            metrics.neg_precision_test=\badprecision,
            metrics.neg_recall_test=\badrecall,
            metrics.pos_precision_test=\topprecision,
            metrics.pos_recall_test=\toprecall,
            metrics.model_size_test=\modelsize,
        }{%
            \circled{\texttt{\IfBeginWith{\experiment}{tensorflow}{T}{K}}} &
            \small\texttt{\StrMid{\runid}{0}{7}} &
            \small\StrBehind[1]{\experiment}{.} &
            $\np{\accuracy}$ &
            $\np{\badprecision}$ &
            $\np{\badrecall}$ &
            $\np{\topprecision}$ &
            $\np{\toprecall}$ &
            \nprounddigits{3}$\np{\modelsize}$
        }
        \caption{TensorFlow: Metrics - Top $10$}\label{table:tf-ktrain-metrics-top}
    \end{table}
}

The model size is illustrated in more detail in the following figure~\ref{fig:tf-klearn-model-sizes}.

Subfigure~(\hyperref[fig:tf-klearn-model-sizes:a]{\texttt{a}}) shows the model size of the top $10$ models.
It is particularly notable that both DistilBERT models have the largest model size with approx. $255.766$~MB.
All remaining models have a much smaller model size.
It is observable that even two models, run \verb|9e1585a| and \verb|aa21e27|, have only a size of slightly over $4$~MB.
Overall, the median model size of the top $10$ models is just $17.758$~MB.

In subfigure~(\hyperref[fig:tf-klearn-model-sizes:b]{\texttt{b}}), it can be seen that nearly all models are located approximately between $10$~MB and $60$~MB.
Only a few outliers can be found at around $250$~MB and $420$~MB.

Subfigure~(\hyperref[fig:tf-klearn-model-sizes:c]{\texttt{c}}) shows two boxplots over all runs distinguished in TensorFlow and ktrain models.
Looking at all runs and omitting individual outliers, we see that the TensorFlow models tend to be smaller than the models learned with ktrain.
Almost $100\%$ of TensorFlow models are between $4$~MB and $20$~MB, while most ktrain models are between $10$~MB and $110$~MB.
Moreover, the median size of TensorFlow models are only about $10$~MB, whereas for ktrain models are about $50$~MB.

\begin{figure}[H]
	\hspace*{\fill}
    \subfloat[Top $10$\label{fig:tf-klearn-model-sizes:a}]{%
        \includesvg[width=.7\textwidth]{plots/tensorflow/tf_ktrain_model_size_top_10_in_mb.svg}
    }
    \hspace*{\fill}\hspace*{\fill}
    \subfloat[Boxplot over all runs\label{fig:tf-klearn-model-sizes:b}]{%
        \includesvg[width=.26\textwidth]{plots/tensorflow/tf_ktrain_model_size_in_mb_boxplot.svg}
        \vspace*{4mm}
    }
    \hspace*{\fill}
	\phantomcaption
\end{figure}\vspace*{-8mm}
\begin{figure}[H]
    \ContinuedFloat
    \centering
    \subfloat[Boxplot over all runs distinguished in \textit{TensorFlow} and \textit{ktrain}\label{fig:tf-klearn-model-sizes:c}]{%
        \includesvg[width=.59\textwidth]{plots/tensorflow/combined_less_model_size_in_mb_boxplot.svg}
    }
	\caption{TensorFlow: Model size in MB}
	\label{fig:tf-klearn-model-sizes}
\end{figure}


The next figure~\ref{fig:tf-klearn-metrics} visualize the metrics of each model.

In subfigure~(\hyperref[fig:tf-klearn-metrics:a]{\texttt{a}}) it can be seen that the best model is very balanced over all metrics.

All other models show the same pattern, with the exception of the penultimate model.
It is illustrated that the \textit{accuracy} score is most of the time bigger than the \textit{bad precision} score.
These metrics are followed by the \textit{bad recall} metric which in most cases records the highest scores.
After that a slightly lower score can be noted for the \textit{top precision}.
The last metric is the \textit{top recall}, which is the worst metric for most of the models.

The penultimate model differs from the other models by having a higher score for \textit{top recall} and \textit{bad precision} instead of \textit{bad recall} and \textit{top precision}.
In fact, the \textit{bad recall} and \textit{top precision} metric are the worst for this model.

Subfigure~(\hyperref[fig:tf-klearn-metrics:b]{\texttt{b}}) shows the boxplots for the individual metrics when all runs are considered.
Overall, it can be seen that the median across all metrics is at $0.8$ or higher.

It is especially noticeable that the metrics \textit{accuracy} and \textit{bad precision} have the only outliers across all metrics.
Here, the number of downward outliers predominates.
Furthermore, it should be pointed out that the best model is an outlier.

Moreover, it is notable that \textit{bad recall} has the biggest dispersion over all metrics.

\begin{figure}[H]
	\centering
    \subfloat[Top $10$\label{fig:tf-klearn-metrics:a}]{%
        \includesvg[width=\textwidth]{plots/tensorflow/tf_ktrain_metrics_top_10.svg}
    }
    \phantomcaption
\end{figure}\vspace*{-8mm}
\begin{figure}[H]
    \ContinuedFloat
    \centering
    \subfloat[Boxplot over all runs\label{fig:tf-klearn-metrics:b}]{%
        \includesvg[width=.8\textwidth]{plots/tensorflow/tf_ktrain_metrics_boxplot.svg}
    }
	\caption{TensorFlow: Metrics}
	\label{fig:tf-klearn-metrics}
\end{figure}

% The last table~(\ref{table:tf-klearn-top-metrics}) shows the best runs for each metric.
% The best accuracy has the DistilBERT model which can also be seen in the table~\ref{table:tf-ktrain-metrics-top}.
% For the \textit{bad precision} the best model is the bidirectional model with $0.968$.
% The best \textit{bad recall} model is a basic model with about $0.984$.
% The next model is also a bidirectional model but is a different run as the previous one and has a \textit{top precision} of like $0.977$.
% One of the DistilBERT models has a \textit{top recall} of $1$ and is simply predicting every domain to be a good domain.
% The last metric is the \textit{model size} where the smallest model is a basic model with an model size of $40.42$~MB.

% {
%     \setlength{\tabcolsep}{5.5pt}
%     \begin{table}[H]
%         \centering
%         \nprounddigits{5}
%         \csvreader[%
%             head to column names,
%             centered tabular=@{\space}c@{\space}lllllllr,
%             table head=~ & ~ & ~ & ~ & \bfseries Bad & \bfseries Bad & \bfseries Top & \bfseries Top & \bfseries Model Size\\
%             ~ & \bfseries Run ID & \bfseries Type & \bfseries Accuracy & \bfseries Precision & \bfseries Recall & \bfseries Precision & \bfseries Recall & \bfseries in MB\\\toprule,
%             late after line=\\, late after last line=\\\bottomrule,
%             respect underscore=true,
%             preprocessor={\csvencode},
%         ]{tables/tensorflow/tf_klearn_top_metrics.csv}{
%             run_id=\runid,
%             metrics.accuracy_test=\accuracy,
%             metrics.neg_precision_test=\badprecision,
%             metrics.neg_recall_test=\badrecall,
%             metrics.pos_precision_test=\topprecision,
%             metrics.pos_recall_test=\toprecall,
%             metrics.model_size_test=\modelsize,
%         }{%
%             \circled{\texttt{\IfBeginWith{\experiment}{tensorflow}{T}{K}}} &
%             \small\texttt{\StrMid{\runid}{0}{7}} &
%             \small\StrBehind[1]{\experiment}{.} &
%             $\accuracy$ &
%             $\badprecision$ &
%             $\badrecall$ &
%             $\topprecision$ &
%             $\toprecall$ &
%             \nprounddigits{3}$\modelsize$
%         }
%         \caption{TensorFlow: Top Metrics}\label{table:tf-klearn-top-metrics}
%     \end{table}
% }
