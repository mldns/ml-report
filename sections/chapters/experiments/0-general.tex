The experiments in this research paper have been conducted on the \hyperref[sec:fasttext]{FastText} and \hyperref[sec:tensorflow]{TensorFlow} platforms and libraries, respectively.

We have chosen FastText and TensorFlow, because
\begin{enumerate}
    \item \hyperref[sec:fasttext]{FastText} leads very quickly to very good results and
    \item \hyperref[sec:tensorflow]{TensorFlow} is extremely flexible and customizable.
\end{enumerate}

More details about the two platforms and libraries can be found in the corresponding experiment.


\subsubsection{Training Environment}

To train our models we used a server with following hardware configuration:
\begin{verbatim}
CPU: Intel(R) Xeon(R) CPU E5-2680 v4 @2.4GHz, 14 Cores per Socket, total 56 cores
GPU: 2x NVIDIA TITAN X (Pascal)
RAM: 264 GB
 OS: Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-151-generic x86_64)
\end{verbatim}


\subsubsection{Metrics}

After training each model was examined by using the test dataset.
The following metrics were used to investigate and compare the different models.

{
    \renewcommand{\arraystretch}{2.5}  % {1.5}
    \begin{tabular}{@{\hspace*{5.75mm}\textbullet~~}l@{~~$=$~~}c@{~~$=$~~}l}
        Accuracy      & $\displaystyle\frac{TP + TN}{TP + TN + FP + FN}$ & $\displaystyle\frac{\text{\small N. of correct predictions}}{\text{\small N. of all predictions}}$\\
        Bad Recall    & $\displaystyle\frac{TN}{TN + FP}$ & $\displaystyle\frac{\text{\small N. of correctly predicted negative instances}}{\text{\small N. of total negative instances in the dataset}}$\\
        Bad Precision & $\displaystyle\frac{TN}{TN + FN}$ & $\displaystyle\frac{\text{\small N. of correctly predicted negative instances}}{\text{\small N. of total negative predictions made}}$\\
        Top Recall    & $\displaystyle\frac{TP}{TP + FN}$ & $\displaystyle\frac{\text{\small N. of correctly predicted positive instances}}{\text{\small N. of total positive instances in the dataset}}$\\
        Top Precision & $\displaystyle\frac{TP}{TP + FP}$ & $\displaystyle\frac{\text{\small N. of correctly predicted positive instances}}{\text{\small N. of total positive predictions made}}$\\
        \multicolumn{3}{@{\hspace*{5.75mm}\textbullet~~}l}{Model Size}
    \end{tabular}
}

The metric \textit{accuracy} is used here to determine how well a model classifies the domains in the test dataset.
However, \textit{accuracy} can be skewed if the test dataset is unbalanced, so the metrics \textit{recall} and \textit{precision} are also used.
These metrics help overcome the problems with \textit{accuracy}, as \textit{precision} shows how well a class is predicted when it is actually that specific class.
\textit{Recall}, on the other hand, shows how many of the domains of a class, good or malicious, the classifier correctly predicted compared to all cases of the same class in the data.

Another important metric is in our case the model size (see section~\ref{sec:prelimit}), since the model has to be able to run on a home router.
The model size is specified in MegaBytes~(MB) and helps to estimate whether a model can be integrated on a router.

In the experiments, the data were sorted by the metrics in the following order:
\begin{enumerate}
    \item Accuracy
    \item Bad Recall
    \item Top Recall
    \item Model Size
\end{enumerate}


\subsubsection{Machine Learning Lifecycle Management}

In this project we used an machine learning managing and logging platform.
At first, the Neptune.ai~\cite{neptune} machine learning management tool was considered, but we decided to use MLflow~\cite{MLflow}, an open-source alternative which is hostable on a dedicated server, for example with a Docker~\cite{merkel2014docker} instance.

\begin{shadequote}[r]{\href{https://mlflow.org}{MLflow}}
    MLflow is an open source platform to manage the ML lifecycle, including experimentation, reproducibility, deployment, and a central model registry.
\end{shadequote}

MLflow tracks experiments and gives an option to compare parameters and results.
It also helps to improve the reusability, reproducibility and comprehensibility of models.
This is accomplished by logging models and its parameters and metrics.
It also comes with some autologging capabilities for example for TensorFlow where the models, parameters and training metrics like accuracy and loss are logged without manual interaction.
Furthermore, it is library-agnostic which means that it can be used with any machine learning library and in any programming language by using the CLI or the RESTful~API.


\subsubsection{Repository Structure}

During the Telekom Challenge, several repositories were created on GitLab.
A complete list of all repositories can be found in the \hyperref[apx:repositories]{appendix}.

At this point, we will only briefly present the repositories that are relevant for this work.

\href{https://gitlab.com/mldns}{Research Project}:
\begin{itemize}
    \item \href{https://gitlab.com/mldns/mlflow-docker}{mlflow-docker}
    \item \href{https://gitlab.com/mldns/facebookresearch/fasttext}{Facebook's AI Research fastText}
    \item \href{https://gitlab.com/mldns/fasttext}{FastText} \href{https://mldns.gitlab.io/fasttext}{[Doc]}
    \item \href{https://gitlab.com/mldns/tensorflow}{TensorFlow} \href{https://mldns.gitlab.io/tensorflow}{[Doc]}
    \item \href{https://gitlab.com/mldns/ml-report}{ML Report}
\end{itemize}


\paragraph{mlflow-docker}
\begin{shadequote}[r]{\href{https://github.com/Toumash/mlflow-docker}{Tomasz Dłuski}}
    Production ready docker-compose configuration for ML Flow with Mysql and Minio S3
\end{shadequote}

MLflow for docker~\cite{mlflow-docker} is a repository that provides a complete MLflow environment with MinIO~\cite{minio}, a multi-cloud object storage, by a simple \verb|docker-compose up|.


\paragraph{Facebook's AI Research fastText}
\begin{shadequote}[r]{\href{https://github.com/facebookresearch/fastText}{Facebook Research}}
    fastText is a library for efficient learning of word representations and sentence classification.
\end{shadequote}

The fastText repository contains the source code from the Facebook library fastText.

In order to have control over the version, as well as the guarantee of availability and moreover the power to change the code itself, we created our own clone.


\paragraph{FastText}
~\\
The FastText Repository covers everything around the experiment~\hyperref[sec:fasttext]{FastText}.
Here you can find the complete source code and a developer documentation.

At this point it should be noted that the spelling is different.
When we talk about \textit{fastText}, we explicitly mean the library from Facebook.
However, if we talk about \textit{FastText}, with a capital \textit{F}, we mean our experiment or something that has been developed from it.


\paragraph{TensorFlow}
~\\
The TensorFlow repository has been used to test various approaches and stores the source code and developer documentation for these experiments.


% \paragraph{Report}
% ~\\
% Finally, we created our own repository for exactly this report.
