\section{Prerequisites \& Limitations}\label{sec:prelimit}

In general, it can be said that this project has specific requirements on the machine learning model.
This has multiple reasons one of which is that the model should be efficient enough to handle domain requests quickly, so it can be used on the router without any further issues.
Also, the model should be small, because the home router does not have much RAM to hold large models in memory.
As a home router replacement we used a Raspberry Pi~4 Model~B (4~GB~RAM) as reference in this project.

An additional requirement is that the classification and therefore blocking is only done locally.
To achieve this, the models have not only to be small but also fast and efficient.

In this project, the main task from a machine learning standpoint was to identify malicious domains.
Therefore, data are needed to apply the different machine learning approaches.
In this case labeled data was used where the blacklist dataset contains domains that are considered malicious (bad) and the whitelist dataset contains domains assumed to be benign (top).

Both, the whitelist and the blacklist, are split into train, validation, and test datasets.
Their respective percentages are $60\%$, $20\%$ and $20\%$.
The whitelists and blacklists datasets are combined and shuffled for train, validation, and test datasets.

The main preprocessing step is to generate \textit{sentences} out of domains.
That means spaces replace every dot, underscore and dash, so that a sentence like \verb|www telekom de| is created.

In the following, we will use the term \textit{domain~word} as a component of these sentences, like \verb|telekom| or \verb|www|.
Sentences are often used in Natural Language Processing (\ref{sec:nlp}), so the following models will produce better results with a sentence structured input.


\subsection{Whitelist}\label{sec:whitelist}

The whitelist dataset contains the Cisco Umbrella Popularity List~\cite{Cisco21}.
This list consists of the most popular $1$ million domains, which are the most queried domains in the Umbrella global network.
The Umbrella global network processes more than $100$ billion queries per day from $65$ million unique active users in more than $165$ countries.
Although the list presents the most popular domains, those are not necessary only benign domains.

The following preprocessing steps are attempted to clean the whitelist of suspicious domains:
\begin{itemize}
    \item remove duplicate domains,
    \item remove invalid \textit{top level domains}~(TLDs),
    \item remove domains that are also part of the blacklist,
    \item remove local domain addresses, e.g. localhost,
    \item remove domains that include UUID as a significant part of the domain,
    \item remove domains that include subdomains with more than $30$ characters, and
    \item attempting to remove \textit{domain generation algorithm}~(DGA) domains.
\end{itemize}

The removal of the DGA domains is done by using a DGA list which was kindly provided by Deutsche Telekom.
Even after the preprocessing, it has to be assumed that some suspicious domains are still included in the list, potentially affecting training of the models.

In the later experiments, we used both versions of the whitelists.
We named the "clean" whitelist \verb|whitelist_cleaned| and the untouched one \verb|whitelist|.

Another challenge is the imbalance in the number of occurrences of entries in the list, illustrated by the table~\ref{tab:occurrences}.

\begin{table}[H]
	\hspace*{\fill}
    \subfloat[Count of occurrences of domain subjects\label{tab:occurrences:a}]{%
        \csvreader[%
            tabular=lr,
            table head=\bfseries Occurrence of domain subjects & \bfseries Count \\\toprule,
            late after line=\\, late after last line=\\\bottomrule,
            respect underscore=true,
            preprocessor={\csvencode},
        ]{tables/data/occurrences-of-subjects.csv}{}{%
            \csvcoli & \Verb|\csvcolii|
        }
    }
    \hspace*{\fill}\hspace*{\fill}
    \subfloat[Domain subject frequency\label{tab:occurrences:b}]{%
        \csvreader[%
            tabular=lr,
            table head=\bfseries Domain subject & \bfseries Frequency \\\toprule,
            late after line=\\, late after last line=\\\bottomrule,
            respect underscore=true,
            preprocessor={\csvencode},
        ]{tables/data/domain-subject-frequency.csv}{}{%
            \csvcoli & \Verb|\csvcolii|
        }
    }
    \hspace*{\fill}
	\caption{Occurrence of domain subjects}\label{tab:occurrences}
\end{table}

Let the subject of a domain sentence be the \textit{second level domain}~(SLD) without its root level, like \verb|google| derived from \verb|maps.google.com|.

The first line in table~\ref{tab:occurrences:a} specifies the number of unique domain subjects.
It can be seen that $84,247$ domain subjects are unique in the whitelist.
The second row shows that $75,969$ domain subjects occur twice.
This could mean that the domain has different \textit{top-level domains}~(TLD) or different subdomain names.
Also, it should be noted that over $50$ percent of the domain subjects are represented more than once.

In table~\ref{tab:occurrences:b} a selection of domain subjects is shown with their corresponding number of occurrences.
\verb|googlevideo|, for example, is contained $27,256$ times in the whitelist.
Also \verb|windows| and \verb|facebook| with $18,012$ and $14,758$ respectively are some of the more common domains which occur.
Furthermore, approximately $96$ percent of all domain subjects occur less than ten times.

Certainly, some repetitions are to be expected, but the extreme differences could potentially decrease the accuracy.
The main risk with using unbalanced data like this is that models could learn that, for example, every domain name including \verb|googlevideo| is a good domain, even if \verb|googlevideo| was only part of a subdomain name.

Overall, it can be concluded that the whitelist used for training is not ideal.
Some data is non-representative, and the overall quality leaves room for improvement.

In the end, it all comes down to the amount and quality of the labeled data available for training.
ISPs have access to a large amount of data, often times labeled, which can be used to further improve the model accuracy.


\newpage
\subsection{Blacklist}\label{sec:blacklist}

The blacklist utilized in this project is a combination of many publicly available sources:
\begin{itemize}
    \item COVID-19 Cyber Threat Coalition \textit{Blocklist}~\cite{COVID21}
    \item Steven Black's \textit{Hosts} file~\cite{Black21}
    \item Université Toulouse 1 Capitole \textit{Blacklist}~\cite{Toulouse21} with the categories malware and phishing
\end{itemize}

All these blacklists were combined and all duplicates were removed.


\subsubsection{COVID-19 Cyber Threat Coalition \textit{Blocklist}}

\begin{shadequote}[r]{\href{https://www.cyberthreatcoalition.org/blocklist}{COVID-19 Cyber Threat Coalition}}
    We publish data sets with indicators we believe to be used by criminals trying to prey on individuals, organizations, businesses and governments using the COVID-19 pandemic.
    We do this because we want to help you stop them.
\end{shadequote}

The COVID-19 Cyber Threat Coalition \textit{Blocklist} contains domains that are considered to be used in the COVID-19 pandemic to do malicious or criminal activities.

The complete blacklist can be found here:\\
\url{https://blocklist.cyberthreatcoalition.org/vetted/domain.txt}


\subsubsection{Steven Black's \textit{Hosts} file}

\begin{shadequote}[r]{\href{https://github.com/StevenBlack/hosts}{Steven Black}}
    This repository consolidates several reputable hosts files, and merges them into a unified hosts file with duplicates removed.
    A variety of tailored hosts files are provided.
\end{shadequote}

Steven Black consolidates and extends hosts files from several well-curated sources on GitHub.
This project uses the most basic variant Unified hosts which includes adware and malware.

The full list of included sources can be found here:\\
\url{https://github.com/StevenBlack/hosts#sources-of-hosts-data-unified-in-this-variant}

The complete blacklist can be found here:\\
\url{https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts}


\subsubsection{Université Toulouse 1 Capitole \textit{Blacklist}}

\begin{shadequote}[r]{\href{http://dsi.ut-capitole.fr/blacklists/index_en.php}{University of Toulouse}}
    The Université Toulouse 1 Capitole propose a blacklist managed by Fabrice Prigent from many years, to help administrator to regulate Internet  use.
\end{shadequote}

The University of Toulouse categorizes domains.
Their main focus lies in the category pornography, but they also maintain other categories.

This project uses the categories malware and phishing.

All blacklists sorted into multiple categories can be found here:\\
\url{http://dsi.ut-capitole.fr/blacklists/index_en.php}
