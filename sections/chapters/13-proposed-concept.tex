\section{Proposed Concept}\label{sec:proposed-concept}

Machine Learning DNS is a solution integrated in the customer router to protect users from accessing malicious domains.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{proposed-concept/mldns-overview.png}
    \caption{Machine Learning DNS Overview}
    \label{fig:mldns-overview}
\end{figure}

\begin{enumerate}
    \item \textbf{Problem}\\
    Cyber attacks are increasing and causing massive damage to affected users.
    While corporate networks have hardened cyber security mechanisms in place to protect themselves against attacks, home users are effectively unprotected.
    Cyber security is expensive and complex, to a degree that even IT professionals find it hard to keep up with.
    Home users stand no chance to achieve a comparable level of IT security in their home networks.
    A simple-to-configure and easy to maintain solution is required.

    \item \textbf{Trust}\\
    \textit{Established Internet service providers}, such as Telekom, are providing customer-premises equipment~(CPE) such as home routers to get connected to the Internet.
    Telekom's portfolio covers broadband, cellular and entertainment - the foundation of digital services.
    With Machine Learning DNS, Telekom extends their portfolio towards cyber security for home networks, delivering customers the unified digital experience.
    Customers get \textit{security from a brand they trust}.

    \item \textbf{Provider}\\
    Telekom provides broadband customers with its branded router.
    Machine Learning DNS is \textit{incorporated into the router firmware}.
    The pre-trained machine learning model is constantly refined with data from the Telekom security team.
    Through training, the prediction quality is \textit{continuously improving}.
    Users receive regular updates of the machine learning model, combined with static blacklists.

    \item \textbf{User}\\
    User's profit from an \textit{unchanged user experience} during normal browsing.
    When a suspicious site is trying to be accessed, for example from a phishing e-mail, Machine Learning DNS provides users with a \textit{warning screen} and an easy-to-use interface to decide how to proceed.
    Users have a good feeling browsing the Internet because they know that Telekom takes care of their security.
\end{enumerate}


\newpage
\subsection{DNS Adaption}\label{sec:dns}

Domain Name Service~(DNS) is an essential Internet service.
DNS also offers excellent opportunities as a security tool.
Machine Learning DNS protects home networks by analyzing the DNS traffic using machine learning techniques.
A classifier is trained to distinguish between clean domains names and malicious domains.
Thus, it extends the performance currently offered by static blacklists.
The preprocessing of the domains as well as the creation of the classifier are done with NLP~(\ref{sec:nlp}) techniques and ideas.
Even that the domains are not necessary a real natural language, the techniques can still applied here because of the similarity of this problem, as can be seen later.
The classifier could be integrated into any widely used DNS software, such as unbound, which should be able to run on a CPE router.
Traffic to domains classified as malicious are blocked.

\begin{figure}[H]
    \centering
    \includesvg[width=\textwidth]{proposed-concept/dns-filter.svg}
    \caption{Home Network Overview with Machine Learning DNS}
    \label{fig:home-network-overview-mldns}
\end{figure}

Domain names are classified as either clean or malicious before the request is sent out to Internet name servers.
Malicious domains could for example be blocked by returning the local address and redirecting the request to a local web service.
The key components of this solution are:
\begin{itemize}
    \item An recursive DNS resolver, like unbound, filters the relevant DNS resource record requests, e.g., A, AAAA, MX, TXT, CNAME.

    \item Machine learning libraries or frameworks like fastText and TensorFlow could be used to train a model that classifies domain names.
    The predicted domains are either clean or malicious, so the result could be returned through the unbound interface.

    \item A local web service could provide a user interface for configuration and could inform an user in case a domain has been blocked.
    In addition, this web service could handle the management of the blacklist and whitelist via an API.
\end{itemize}


\newpage
\subsection{Concept}

\begin{minipage}[c]{0.5\linewidth}
    Our idea can be compared figuratively with a sieve.
    This sieve is designed to filter out malicious domains before the actual DNS resolution.
    The appropriate place to integrate this sieve would be the gateway to the Internet, the home router.

    In contrast to many other systems, which only analyze the results, this would provide preventive protection against malicious domains.
    This means that in the ideal case malicious domains will never be resolved and the actual request would never leave the home network.

    The sieve also represent a neural network.
    This neural network distinguish domains into good and malicious ones.
    Malicious classified domains could then be easily blocked.
\end{minipage}
\hspace*{\fill}
\begin{minipage}[c]{0.4\linewidth}
    \begin{figure}[H]
        \centering
        \includegraphics[width=.8\textwidth]{proposed-concept/sieve-analogy.png}
        \caption[Sieve analogy for MLDNS]{Sieve analogy for MLDNS\protect\footnotemark}
        \label{fig:sieve-analogy}
    \end{figure}
\end{minipage}
\footnotetext{Visuals from \url{https://www.flaticon.com}}

~\\
In the following figure~\ref{fig:web-protection-concepts} we can see a comparison between the web protection concept nowadays and our proposed solution.

Currently, a DNS resolution goes through the following checks:
\begin{enumerate}
    \item personal blacklist and whitelist \textit{(optional)}
    \item blacklist and whitelist of the provider
\end{enumerate}
If domains do not appear on the personal or provider maintained lists, which is true for most domains, especially for new domains, they are always resolved and classified as benign domains by default.

This is precisely where we want to start.
An unknown domain should not be classified as a benign domain by default.
The best concept would be, if only domains would be resolved, which have been extensively checked before.
In this case, we could only work with whitelists.
Unfortunately, this approach is not really feasible.
For this reason, we propose that an AI takes over the classification of unknown domains.

\begin{figure}[H]
	\subfloat[Web protection today]{%
        \includesvg[width=.4\textwidth]{proposed-concept/web-protection-today.drawio.svg}
    }
    \hspace*{\fill}
    \subfloat[Web protection with MLDNS]{%
        \includesvg[width=.45\textwidth]{proposed-concept/web-protection-mldns.drawio.svg}
    }
    \caption{Web protection concepts}
	\label{fig:web-protection-concepts}
\end{figure}


\newpage
\subsection{Key Features}

Home networks are continuously growing.
More than ten connected devices, computers, TVs, smartphones, and IoT systems are common, and in the future, we may expect hundreds of devices in a single home network.
Only a tiny subset of the devices run a well-known operating system and receive regular security updates.
We can no longer assume that all devices in the home network are trustworthy.

However, home networks are subject to various restrictions compared to enterprise networks:
\begin{itemize}
    \item only basic administration and configuration
    \item no dedicated security devices are available
    \item budget restrictions
    \item power consumption
    \item data privacy must be ensured
\end{itemize}

The CPE router is the central device that can be used for security services.
Anyway, any extension must be lightweight and user-friendly.
It should also not interfere with the core functionality of the router, such as Internet and multimedia access.
As a central component, the router can help to protect all devices in the home network.

By filtering DNS requests, we can effectively control the traffic and block malicious domains, e.g., command and control~(C\&C) traffic or downloads from rogue sites.


\subsubsection{Privacy}

The integration into local end-user equipment offers significant privacy advantages compared to centralized filtering solutions.

\begin{itemize}
    \item Connections to malicious domains are blocked locally.
    \item External entities cannot log blocked requests.
    \item DNS logs are sensitive data and the end-users is able to remain in complete control of their personal data.
\end{itemize}


\subsubsection{High Performance}

Since DNS traffic plays such an essential role in the daily Internet traffic of a typical household, high performance is necessary.
Every component of Machine Learning DNS works on low-power hardware and introduces no negative impact on the end-user experience.
Particular attention is paid to the latency of DNS resolution.
The overall system latency must not be noticeably affected by MLDNS.
It is also important to keep in mind that the end device is limited in terms of resources.
In order to achieve this, special attention was paid to the following:
\begin{itemize}
    \item low latency prediction
    \item low latency blacklist and whitelist management
    \item small model size
\end{itemize}
