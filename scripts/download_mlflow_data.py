#!/usr/bin/env python
# cspell:words mlflow inplace
############################################################
# Download all experiments
# and save them as sorted csv files.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import requests
import pandas as pd
import logging

from sys import exit
from os import environ

from collections.abc import Iterable

from scripts.experiments import Experiment, foreach_experiment, load_experiment_config, _pretty_print_json


def _server_reachable() -> bool:
    """
    Check if the server is reachable.

    Returns
    -------
    bool
        `True` if server reachable, otherwise `False`.
    """
    tracking_url = environ.get('MLFLOW_TRACKING_URI')
    try:
        r = requests.head(tracking_url, timeout=1)
        return r.status_code == 200
    except:
        return False


def download_experiments(experiments: dict) -> None:
    """
    Download experiments (inplace).

    Arguments
    ---------
    experiments: dict
        Experiments dictionary.
    """
    def flatten(d: dict, *, sep: str = '.', parent_key: str = '', _level: int = 0) -> dict:
        """
        Flatten mlflow experiment data response.

        Arguments
        ---------
        d: dict
            MLflow experiment data response.
        sep: str, optional
            Key seperator.
        parent_key: str, optional
            Parent key or prefix with will be concatinated with the seperator and new key.
        _level: int, optional
            Dictionary depth index.

        Returns
        -------
        dict
            Flatten dictionary.
        """
        items = []
        for key, value in d.items():
            new_key = f"{parent_key}{sep}{key}" if parent_key else key
            if _level == 0 and key in [ 'info', 'data' ] and isinstance(value, Iterable):
                # remove unnecessary key prefix 'info' and 'data
                new_key = ''
            if isinstance(value, dict):
                items.extend(flatten(value, sep=sep, parent_key=new_key, _level=_level+1).items())
                continue
            if isinstance(value, list):
                for subdict in value:
                    items.extend(flatten(
                        { subdict['key']: subdict['value'] },
                        sep=sep,
                        parent_key=new_key,
                        _level=_level+1
                    ).items())
                continue
            items.append((new_key, value))
        return dict(items)

    def download(experiment: Experiment) -> None:
        """
        Download experiment, convert it to an pandas data frame and
        save the result inside the experiment object.
        This method works inplace!

        Arguments
        ---------
        experiment: Experiment
            Experiment which should be downloaded and altered.
        """
        data = get_experiment(experiment['id'])
        runs = [ flatten(run) for run in data.get('runs', []) ]
        experiment['data'] = pd.DataFrame(runs)

    foreach_experiment(experiments, download)


def get_experiment(id: int) -> dict:
    """
    Get experiment data by id.

    Arguments
    ---------
    id: int
        MLflow experiment ID.

    Returns
    -------
    dict
        MLflow experiment data.
    """
    tracking_url = environ.get('MLFLOW_TRACKING_URI')
    req = requests.get(f"{tracking_url}/ajax-api/2.0/preview/mlflow/runs/search?experiment_ids={id}")
    return req.json()


def main() -> None:
    """
    Main entry for the report plot generator script.
    """
    logging.basicConfig(level=environ.get("LOGLEVEL", "INFO"))
    if not _server_reachable():
        logging.error(f"Server unreachable.")
        exit(1)

    experiments = load_experiment_config()
    #_pretty_print_json(experiments)

    download_experiments(experiments)
    #print(experiments['fasttext']['data'])

    foreach_experiment(experiments, lambda experiment: [
        experiment.sort(),
        experiment.export()
    ])

    # load and sort fasttext data source csv files
    df = pd.read_csv('./data/ft_data_source.csv')

    d = {
        'lower_case_split_by_dot_dash_underscore': 'A',
        'trim_tailing_dot': 'B',
        'trim_leading_www': 'C',
        'domain_level_2': 'D',
        'domain_level_3': 'E',
        'max_length_30': 'F',
        'max_length_40': 'G',
        'max_length_50': 'H',
        'max_length_100': 'I',
        'max_word_length_1': 'J',
        'max_word_length_3': 'K',
        'max_word_length_5': 'L',
        'max_word_length_10': 'M',
        'remove_tld': 'N',
    }
    for key, value in d.items():
        df['data_preprocessing'] = df['data_preprocessing'].apply(
            lambda x: x.replace(key, value)
        )
    df['data_preprocessing'] = df['data_preprocessing'].apply(
        lambda x: ','.join(sorted([ y.strip() for y in x.split(',') ]))
    )

    df = df.set_index('run_id')
    df = df.reindex(index=experiments['fasttext']['data']['run_id'])
    df = df.reset_index()
    df.to_csv('./data/ft_data_source.csv', index=False, header=True)


if __name__ == '__main__':
    main()
