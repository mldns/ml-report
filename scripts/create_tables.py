#!/usr/bin/env python
# cspell:words idxmax idxmin iloc ngrams klearn
# cspell:enableCompoundWords
############################################################
# Create additional csv data tables.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import pandas as pd

from os import makedirs, path

from scripts.experiments import Experiment, foreach_experiment, load_experiment_config


def top_metrics_df(
    df: pd.DataFrame,
    metrics: list,
    *,
    metrics_asc: list = None,
    highlighting: str = None
) -> pd.DataFrame:
    """
    Create data frame with top metrics.

    Arguments
    ---------
    df: pd.DataFrame
        Data frame.
    metrics: list
        Metrics column names.
    metrics_asc: list, optional
        List of boolean per metric column.
        If `True` maximum will used as top; otherwise minimum.
        Default is `True` for each column.
    highlighting: str, optional
        LaTeX highlighting color to add for "top" metric value.
        If `None` no highlighting will applied.

    Returns
    -------
    pd.DataFrame
        Data frame with only top metrics.
    """
    metrics_asc = metrics_asc if metrics_asc else [True] * df.shape[0]

    rows = { }
    for metric, asc in zip(metrics, metrics_asc):
        idx = df[metric].idxmax() if asc else df[metric].idxmin()
        rows[idx] = rows.get(idx, [])
        rows[idx].append(metric)

    out = pd.DataFrame()
    for idx, columns in rows.items():
        row = df.iloc[[idx]].copy()
        if highlighting:
            for metric in metrics:
                row[metric] = row[metric].apply(
                    lambda x: '{\\np{' + f"{x:<0.8f}" + '}}'
                )
            for column in columns:
                row[column] = row[column].apply(
                    lambda x: '{\\mathcolorbox{' + highlighting + '}{' + str(x) + '}}'
                )
        out = pd.concat([out, row])

    return out


def fasttext(fasttext: Experiment, pca: Experiment) -> None:
    """
    Create additional csv data tables for the fasttext experiment.

    Arguments
    ---------
    fasttext: Experiment
        FastText experiment.
    pca: Experiment
        PCA experiment.
    """
    # top metric runs
    metrics = [
        'metrics.accuracy',
        'metrics.bad_precision',
        'metrics.bad_recall',
        'metrics.top_precision',
        'metrics.top_recall',
    ]
    metrics_asc = [True, True, True, True, True]

    # FastText
    df = top_metrics_df(
        fasttext['data'],
        metrics + ['metrics.model_size_in_mb'],
        metrics_asc=metrics_asc + [False],
        highlighting='yellow'
    )
    csv_path = path.join('tables', 'fasttext', 'ft_top_metrics.csv')
    makedirs(path.dirname(csv_path), exist_ok=True)
    df[metrics + [
        'metrics.model_size_in_mb',
        'run_id',
        'params.epoch',
        'params.dim',
        'params.seed',
        'params.wordNgrams',
        'params.manual set training params'
    ]].to_csv(csv_path, index=False, header=True)

    # pca
    df = top_metrics_df(
        pca['data'],
        metrics,
        metrics_asc=metrics_asc,
        highlighting='yellow'
    )
    csv_path = path.join('tables', 'fasttext', 'pca_top_metrics.csv')
    makedirs(path.dirname(csv_path), exist_ok=True)
    df[metrics + ['run_id', 'params.FastText Run ID']].to_csv(csv_path, index=False, header=True)


def tensorflow(
    tensorflow: Experiment,
    klearn: Experiment,
    tensorflow_klearn: Experiment
) -> None:
    """
    Create additional csv data tables for the tensorflow experiment.

    Arguments
    ---------
    tensorflow: Experiment
        Merged tensorflow experiment.
    klearn: Experiment
        Merged klearn experiment.
    tensorflow_klearn: Experiment
        Merged tensorflow and klearn experiment.
    """
    metrics = [
        'metrics.accuracy_test',
        'metrics.neg_precision_test',
        'metrics.neg_recall_test',
        'metrics.pos_precision_test',
        'metrics.pos_recall_test',
        'metrics.model_size_test',
    ]
    metrics_asc = [True, True, True, True, True, False]
    # tensorflow
    df = top_metrics_df(
        tensorflow['data'],
        metrics,
        metrics_asc=metrics_asc,
        highlighting='green'
    )
    csv_path = path.join('tables', 'tensorflow', 'tf_top_metrics.csv')
    makedirs(path.dirname(csv_path), exist_ok=True)
    df[metrics + ['run_id', 'experiment']].to_csv(csv_path, index=False, header=True)

    # klearn
    df = top_metrics_df(
        klearn['data'],
        metrics,
        metrics_asc=metrics_asc,
        highlighting='green'
    )
    csv_path = path.join('tables', 'tensorflow', 'klearn_top_metrics.csv')
    makedirs(path.dirname(csv_path), exist_ok=True)
    df[metrics + ['run_id', 'experiment']].to_csv(csv_path, index=False, header=True)

    # tensorflow & klearn
    df = top_metrics_df(
        tensorflow_klearn['data'],
        metrics,
        metrics_asc=metrics_asc,
        highlighting='green'
    )
    csv_path = path.join('tables', 'tensorflow', 'tf_klearn_top_metrics.csv')
    makedirs(path.dirname(csv_path), exist_ok=True)
    df[metrics + ['run_id', 'experiment']].to_csv(csv_path, index=False, header=True)


def main() -> None:
    """
    Main entry for additional csv data tables creation script.
    """
    # load experiment data
    experiments = load_experiment_config()
    foreach_experiment(experiments, lambda experiment: experiment.load())

    # create fasttext experiment tables.
    fasttext(experiments['fasttext'], experiments['pca'])

    # load merged experiment data
    merged_experiments = {
        'tensorflow': Experiment(-1, './data/experiments/tensorflow.csv'),
        'klearn': Experiment(-1, './data/experiments/klearn.csv'),
        'tensorflow_klearn': Experiment(-1, './data/experiments/tensorflow_klearn.csv')
    }
    foreach_experiment(merged_experiments.values(), lambda experiment: experiment.load())

    # create tensorflow experiment tables.
    tensorflow(
        merged_experiments['tensorflow'],
        merged_experiments['klearn'],
        merged_experiments['tensorflow_klearn']
    )


if __name__ == '__main__':
    main()
