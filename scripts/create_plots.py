#!/usr/bin/env python
# cspell:words pyplot xlim ylim astype barh vbarplot arange
# cspell:words xticklabels yaxis zorder xticks xaxis bplot
# cspell:words ktrain klearn grure bigru ncol
# cspell:enableCompoundWords
############################################################
# Create report plots.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import matplotlib.pyplot as plt
import numpy as np

from os import makedirs, path

from scripts.experiments import Experiment, foreach_experiment, load_experiment_config


def vbarplot(
    data,
    title: str,
    save_path: str,
    *,
    figsize: (float, float) = (10, 5),
    ylim: (float, float) = None,
    xticklabels: list = None,
    barlabels: list = None,
    colors: list = None,
    legend_kwargs: dict = {},
    **kwargs
) -> None:
    """
    Vertical bar plot.

    Arguments
    ---------
    data: list
        Bar values (1D) or list of bar values (2D).
    title: str
        Plot title.
    save_path: str
        File path  to save plot.
    figsize: tuple, optional
        Figure size.
    ylim: tuple, optional
        Y-axes limitation.
    xticklabels: list, optional
        Labels of the x-axes.
    barlabels: str, list, optional
        Bar label(s).
    colors: str, list, optional
        Bar color(s).
    legend_kwargs: dict, optional
        Additional legend arguments.
        If `None` legend will be skipped.
    kwargs: dict, optional
        Additional bar plot arguments.
    """
    data = np.array(data)
    if len(data.shape) < 2:
        data = np.array([data])
    X = np.arange(len(data[0]))
    if isinstance(barlabels, str):
        barlabels = [ barlabels ]
    if not barlabels: barlabels = [ None ] * len(X)
    if isinstance(colors, str):
        colors = [ colors ]
    if not colors: colors = [ None ] * len(X)

    if 'edgecolor' not in kwargs: kwargs['edgecolor'] = 'gray'

    bargroupsize = 0.9  # size of all bars together in percent (single group)
    barcount = len(data)  # amount of bars (single group)
    barsize = bargroupsize / barcount  # width of a single bar
    barposoffset = -barsize * barcount / 2.0 + barsize / 2.0  # next bar position offset

    _, ax = plt.subplots(figsize=figsize)
    ax.set_title(title)
    if ylim:
        ax.set_ylim(ylim)
    ax.yaxis.grid(zorder=0)

    for date, color, label in zip(data, colors, barlabels):
        if 'zorder' not in kwargs: kwargs['zorder'] = 3
        if label: kwargs['label'] = label
        if color: kwargs['color'] = color
        kwargs['width'] = barsize

        ax.bar(
            X + barposoffset,
            date,
            **kwargs
        )

        barposoffset += barsize

    ax.set_xticks(X)
    if xticklabels:
        ax.set_xticklabels(xticklabels)
    if legend_kwargs is not None:
        ax.legend(**legend_kwargs)

    makedirs(path.dirname(save_path), exist_ok=True)
    plt.savefig(save_path, bbox_inches='tight')
    plt.close()


def bar_plot(
    x,
    y,
    title: str,
    save_path: str,
    *,
    xlim_min: float = 0.7,
    xlim_max: float = 1,
    sort: bool = True,
    reverse: bool = False,
    figsize: (float, float) = (10, 5),
    textpos: float = 12
) -> None:
    """
    Create a metric bar plot.

    Arguments
    ---------
    x: array
        Labels for the metric values.
    y: array
        Metric values.
    title: str
        Plot title.
    save_path: str
        File path  to save plot.
    xlim_min: float, optional
        X-axes minimum limitation.
    xlim_max: float, optional
        X-axes maximum limitation.
    sort: bool, optional
        Sort metrics.
    reverse: bool, optional
        Reverse sort order.
    figsize: tuple, optional
        Figure size.
    textpos: float, optional
        Manipulate bar text position.
    """
    # force metric numbers to be numbers instead of strings
    y = np.array(y).astype(np.float64)

    # sort
    if sort:
        y, x = zip(*sorted(zip(y, x), reverse=reverse))

    # create figure
    fig = plt.figure(figsize=figsize)
    ax = plt.subplot(111)
    ax.barh(x, y, align="center", zorder=3, color='tab:blue')  #, edgecolor='gray')
    ax.set_xlim((xlim_min, xlim_max))
    ax.xaxis.grid(zorder=0)
    plt.title(title)
    plt.tight_layout()

    # add bar labels with each metric number
    for i, v in enumerate(y):
        color = "black"
        pos = v
        if v - ((xlim_max - xlim_min) / textpos) > xlim_min:
            pos = v - ((xlim_max - xlim_min) / textpos)
            color = "white"
        ax.text(
            pos,
            i,
            " " + str(round(v, 3)),
            color=color,
            va="center",
            fontweight="bold",
            zorder=5
        )

    makedirs(path.dirname(save_path), exist_ok=True)
    plt.savefig(save_path, bbox_inches='tight')
    plt.close()


def boxplot(
    data,
    title: str,
    save_path: str,
    *,
    figsize: (float, float) = (4, 5),
    colors: list = [],
    **kwargs
) -> None:
    """
    Create a boxplot.

    Arguments
    ---------
    data: array
        The input data.
    title: str
        Plot title.
    save_path: str
        File path  to save plot.
    figsize: tuple, optional
        Figure size.
    colors: str, list, optional
        Box colors.
    kwargs: dict, optional
        Additional boxplot arguments.
    """
    _, ax = plt.subplots(figsize=figsize)
    ax.set_title(title)

    if 'flierprops' not in kwargs: kwargs['flierprops'] = { 'markerfacecolor': 'g', 'marker': 'D' }
    if 'widths' not in kwargs: kwargs['widths'] = 0.5
    if 'zorder' not in kwargs: kwargs['zorder'] = 3
    if colors: kwargs['patch_artist'] = True
    if not kwargs.get('patch_artist', False):
        # add white box background (of boxplot) to paint over y-axis grid
        kwargs['patch_artist'] = True
        colors = [ 'white' ] * len(data)

    bplot = ax.boxplot(data, **kwargs)

    # hide ticks if there are no labels
    if 'labels' not in kwargs:
        ax.get_xaxis().set_visible(False)

    # add horizontal grid lines
    ax.yaxis.grid(zorder=0)

    # fill with colors
    if colors:
        for box, color in zip(bplot['boxes'], colors):
            box.set(facecolor=color)

    makedirs(path.dirname(save_path), exist_ok=True)
    plt.savefig(save_path, bbox_inches='tight')
    plt.close()


def main() -> None:
    """
    Main entry for the report plot generator script.
    """
    # load experiment data
    experiments = load_experiment_config()
    foreach_experiment(experiments, lambda experiment: experiment.load())

    # load merged experiment data
    merged_experiments = {
        'tensorflow': Experiment(-1, './data/experiments/tensorflow.csv'),
        'ktrain': Experiment(-1, './data/experiments/klearn.csv'),
        'tensorflow_ktrain': Experiment(-1, './data/experiments/tensorflow_klearn.csv')
    }
    foreach_experiment(merged_experiments.values(), lambda experiment: experiment.load())

    # FastText
    ###########
    # Top 10: Model size in MB
    bar_plot(
        [ sha[:7] for sha in experiments['fasttext']['data']['run_id'][:10][::-1] ],
        experiments['fasttext']['data']['metrics.model_size_in_mb'][:10][::-1],
        'Top 10: Model size in MB',
        path.join('plots', 'fasttext', 'model_size_top_10_in_mb.svg'),
        sort=False,
        figsize=(6, 5),
        textpos=7.0,
        xlim_min=0,
        xlim_max=max(experiments['fasttext']['data']['metrics.model_size_in_mb'][:10]) + 50
    )
    # Boxplot: Model size in MB
    boxplot(
        experiments['fasttext']['data']['metrics.model_size_in_mb'],
        'Model size in MB',
        path.join('plots', 'fasttext', 'model_size_in_mb_boxplot.svg'),
        figsize=(2, 5),
    )
    # Top 10: Metrics multi bar plot
    vbarplot(
        [
            experiments['fasttext']['data']['metrics.accuracy'][:10],
            experiments['fasttext']['data']['metrics.bad_precision'][:10],
            experiments['fasttext']['data']['metrics.bad_recall'][:10],
            experiments['fasttext']['data']['metrics.top_precision'][:10],
            experiments['fasttext']['data']['metrics.top_recall'][:10],
        ],
        'Top 10: Metrics',
        path.join('plots', 'fasttext', 'metrics_top_10.svg'),
        ylim=(
            min(
                *experiments['fasttext']['data']['metrics.accuracy'][:10],
                *experiments['fasttext']['data']['metrics.bad_precision'][:10],
                *experiments['fasttext']['data']['metrics.bad_recall'][:10],
                *experiments['fasttext']['data']['metrics.top_precision'][:10],
                *experiments['fasttext']['data']['metrics.top_recall'][:10],
            ) - 0.05,
            1
        ),
        xticklabels=[ sha[:7] for sha in experiments['fasttext']['data']['run_id'][:10] ],
        barlabels=[ 'Accuracy', 'Bad Precision', 'Bad Recall', 'Top Precision', 'Top Recall' ],
        colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki']
    )
    # metrics multi boxplot
    boxplot(
        [
            experiments['fasttext']['data']['metrics.accuracy'],
            experiments['fasttext']['data']['metrics.bad_precision'],
            experiments['fasttext']['data']['metrics.bad_recall'],
            experiments['fasttext']['data']['metrics.top_precision'],
            experiments['fasttext']['data']['metrics.top_recall'],
        ],
        'Metrics',
        path.join('plots', 'fasttext', 'metrics_boxplot.svg'),
        figsize=(8, 5),
        labels=[
            'Accuracy',
            'Bad\nPrecision',
            'Bad\nRecall',
            'Top\nPrecision',
            'Top\nRecall',
        ],
        colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki']
    )

    # PCA
    ######
    # Top 10: Metrics multi bar plot
    vbarplot(
        [
            experiments['pca']['data']['metrics.accuracy'][:10],
            experiments['pca']['data']['metrics.bad_precision'][:10],
            experiments['pca']['data']['metrics.bad_recall'][:10],
            experiments['pca']['data']['metrics.top_precision'][:10],
            experiments['pca']['data']['metrics.top_recall'][:10],
        ],
        'Top 10: Metrics',
        path.join('plots', 'pca', 'pca_metrics_top_10.svg'),
        ylim=(
            min(
                *experiments['pca']['data']['metrics.accuracy'][:10],
                *experiments['pca']['data']['metrics.bad_precision'][:10],
                *experiments['pca']['data']['metrics.bad_recall'][:10],
                *experiments['pca']['data']['metrics.top_precision'][:10],
                *experiments['pca']['data']['metrics.top_recall'][:10],
            ) - 0.05,
            1
        ),
        xticklabels=[ sha[:7] for sha in experiments['pca']['data']['run_id'][:10] ],
        barlabels=[ 'Accuracy', 'Bad Precision', 'Bad Recall', 'Top Precision', 'Top Recall' ],
        colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki']
    )
    # metrics multi boxplot
    boxplot(
        [
            experiments['pca']['data']['metrics.accuracy'],
            experiments['pca']['data']['metrics.bad_precision'],
            experiments['pca']['data']['metrics.bad_recall'],
            experiments['pca']['data']['metrics.top_precision'],
            experiments['pca']['data']['metrics.top_recall'],
        ],
        'Metrics',
        path.join('plots', 'pca', 'pca_metrics_boxplot.svg'),
        figsize=(8, 5),
        labels=[
            'Accuracy',
            'Bad\nPrecision',
            'Bad\nRecall',
            'Top\nPrecision',
            'Top\nRecall',
        ],
        colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki']
    )

    # TensorFlow
    #############
    for key in merged_experiments.keys():
        name = key.replace('tensorflow', 'tf')
        for num in [5, 10]:
            # Top 5 and 10: Model size in MB
            bar_plot(
                [ sha[:7] for sha in merged_experiments[key]['data']['run_id'][:num][::-1] ],
                merged_experiments[key]['data']['metrics.model_size_test'][:num][::-1],
                f"Top {num}: Model size in MB",
                path.join('plots', 'tensorflow', f"{name}_model_size_top_{num}_in_mb.svg"),
                sort=False,
                figsize=(6, 5 / (10/num)),
                textpos=7.0,
                xlim_min=0,
                xlim_max=max(merged_experiments[key]['data']['metrics.model_size_test'][:num]) + 50
            )
            # Boxplot: Model size in MB
            boxplot(
                merged_experiments[key]['data']['metrics.model_size_test'],
                'Model size in MB',
                path.join('plots', 'tensorflow', f"{name}_model_size_in_mb_boxplot{'' if num == 10 else '_small'}.svg"),
                figsize=(2, 5 / (10/num)),
            )

            # Top 5 and 10: Metrics multi bar plot
            vbarplot(
                [
                    merged_experiments[key]['data']['metrics.accuracy_test'][:num],
                    merged_experiments[key]['data']['metrics.neg_precision_test'][:num],
                    merged_experiments[key]['data']['metrics.neg_recall_test'][:num],
                    merged_experiments[key]['data']['metrics.pos_precision_test'][:num],
                    merged_experiments[key]['data']['metrics.pos_recall_test'][:num],
                ],
                f"Top {num}: Metrics",
                path.join('plots', 'tensorflow', f"{name}_metrics_top_{num}.svg"),
                ylim=(
                    min(
                        *merged_experiments[key]['data']['metrics.accuracy_test'][:num],
                        *merged_experiments[key]['data']['metrics.neg_precision_test'][:num],
                        *merged_experiments[key]['data']['metrics.neg_recall_test'][:num],
                        *merged_experiments[key]['data']['metrics.pos_precision_test'][:num],
                        *merged_experiments[key]['data']['metrics.pos_recall_test'][:num],
                    ) - 0.05,
                    1
                ),
                xticklabels=[ sha[:7] for sha in merged_experiments[key]['data']['run_id'][:num] ],
                barlabels=[ 'Accuracy', 'Bad Precision', 'Bad Recall', 'Top Precision', 'Top Recall' ],
                colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki']
            )
            # metrics multi boxplot
            boxplot(
                [
                    merged_experiments[key]['data']['metrics.accuracy_test'],
                    merged_experiments[key]['data']['metrics.neg_precision_test'],
                    merged_experiments[key]['data']['metrics.neg_recall_test'],
                    merged_experiments[key]['data']['metrics.pos_precision_test'],
                    merged_experiments[key]['data']['metrics.pos_recall_test'],
                ],
                'Metrics',
                path.join('plots', 'tensorflow', f"{name}_metrics_boxplot{'' if num == 10 else '_small'}.svg"),
                figsize=(8, 5 / (10/num)),
                labels=[
                    'Accuracy',
                    'Bad\nPrecision',
                    'Bad\nRecall',
                    'Top\nPrecision',
                    'Top\nRecall',
                ],
                colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki']
            )
    # Boxplot: Model size in MB
    boxplot(
        [
            merged_experiments['tensorflow']['data']['metrics.model_size_test'],
            merged_experiments['ktrain']['data']['metrics.model_size_test'],
            merged_experiments['tensorflow_ktrain']['data']['metrics.model_size_test'],
        ],
        'Model size in MB',
        path.join('plots', 'tensorflow', 'combined_model_size_in_mb_boxplot.svg'),
        figsize=(8, 5),
        labels=[
            'TensorFlow',
            'ktrain',
            'TensorFlow & ktrain',
        ],
    )
    boxplot(
        [
            merged_experiments['tensorflow']['data']['metrics.model_size_test'],
            merged_experiments['ktrain']['data']['metrics.model_size_test'],
        ],
        'Model size in MB',
        path.join('plots', 'tensorflow', 'combined_less_model_size_in_mb_boxplot.svg'),
        figsize=(5.5, 5),
        labels=[
            'TensorFlow',
            'ktrain',
        ],
    )

    # Discussion
    #############
    boxplot(
        [
            experiments['fasttext']['data']['metrics.model_size_in_mb'],
            merged_experiments['tensorflow_ktrain']['data']['metrics.model_size_test'],
        ],
        'Model size in MB',
        path.join('plots', 'discussion', 'compare_model_size_in_mb_boxplot.svg'),
        figsize=(5.5, 5),
        labels=[
            'FastText',
            'TensorFlow',
        ],
    )
    # FastText: Top X: Metrics multi bar plot
    num = 4
    vbarplot(
        [
            experiments['fasttext']['data']['metrics.accuracy'][:num],
            experiments['fasttext']['data']['metrics.bad_precision'][:num],
            experiments['fasttext']['data']['metrics.bad_recall'][:num],
            experiments['fasttext']['data']['metrics.top_precision'][:num],
            experiments['fasttext']['data']['metrics.top_recall'][:num],
        ],
        f"Top {num}: Metrics",
        path.join('plots', 'discussion', f"ft_metrics_top_{num}.svg"),
        ylim=(
            0.75,
            1
        ),
        figsize=(4, 5),
        xticklabels=[ sha[:7] for sha in experiments['fasttext']['data']['run_id'][:num] ],
        barlabels=[ 'Accuracy', 'Bad Precision', 'Bad Recall', 'Top Precision', 'Top Recall' ],
        colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki'],
        legend_kwargs={ "loc": "lower left", "mode": "expand", "ncol": 2 }
    )
    # TensorFlow: Top X: Metrics multi bar plot
    vbarplot(
        [
            merged_experiments[key]['data']['metrics.accuracy_test'][:num],
            merged_experiments[key]['data']['metrics.neg_precision_test'][:num],
            merged_experiments[key]['data']['metrics.neg_recall_test'][:num],
            merged_experiments[key]['data']['metrics.pos_precision_test'][:num],
            merged_experiments[key]['data']['metrics.pos_recall_test'][:num],
        ],
        f"Top {num}: Metrics",
        path.join('plots', 'discussion', f"tf_metrics_top_{num}.svg"),
        ylim=(
            0.75,
            1
        ),
        figsize=(4, 5),
        xticklabels=[ sha[:7] for sha in merged_experiments[key]['data']['run_id'][:num] ],
        barlabels=[ 'Accuracy', 'Bad Precision', 'Bad Recall', 'Top Precision', 'Top Recall' ],
        colors=['pink', 'lightblue', 'peachpuff', 'lightgreen', 'khaki'],
        legend_kwargs={ "loc": "lower left", "mode": "expand", "ncol": 2 }
    )


if __name__ == '__main__':
    main()
