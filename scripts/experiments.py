#!/usr/bin/env python
# cspell:words mlflow reindex
############################################################
# Experiments helper classes and methods.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import pandas as pd
import logging

import json

from sys import exit
from os import makedirs, path

from types import FunctionType
from collections.abc import Iterable


class Experiment(dict):
    """
    Experiment data class.
    """

    def __init__(self, id: int, path: str, *, data: pd.DataFrame = None, sort: dict = None):
        """
        Initialize new experiment object.

        Arguments
        ---------
        id: int
            Experiment id.
        path: str
            Experiment data file path.
        data: pandas.DataFrame, optional
            Experiment data.
        sort: dict, optional
            Pandas frame `sort_values` arguments.
        """
        super().__init__({
            'id': id,
            'path': path,
        })
        if data is not None: self['data'] = data
        if sort is not None: self['sort'] = sort

    def export(self) -> None:
        """
        Export data to csv file.
        """
        if 'data' not in self:
            logging.warning(f"Experiment:No data to export. {self}")
            return
        if self['data'].empty:
            logging.warning(f"Experiment:No data to export. Dataframe is empty. {self}")
            return

        makedirs(path.dirname(self['path']), exist_ok=True)
        self['data'].to_csv(self['path'], index=False, header=True)
        logging.info(f"Experiment:Data exported. {self}")

    def sort(self) -> None:
        """
        Sort data.
        """
        if 'data' not in self:
            logging.warning(f"Experiment:No data to sort. {self}")
            return
        if self['data'].empty:
            logging.warning(f"Experiment:No data to sort. Dataframe is empty. {self}")
            return

        self['data'] = self['data'].reindex(columns=sorted(self['data'].columns.to_list()))
        if 'sort' in self and self['sort']:
            self['data'] = self['data'].sort_values(**self['sort'])
        logging.info(f"Experiment:Data sorted. {self}")

    def load(self) -> pd.DataFrame:
        """
        Load csv file.

        Returns
        -------
        pd.DataFrame
            CSV file as pandas data frame.
        """
        if not path.exists(self['path']):
            logging.warning(f"Experiment:Load error. File not found. {self}")
            return None
        self['data'] = pd.read_csv(self['path'])
        return self['data']

    def __str__(self) -> str:
        """
        Get string representation of experiment object.

        Returns
        -------
        str
            String representation of experiment object.
        """
        return f"[id: {self['id']}, path: {self['path']}]"


def foreach_experiment(experiments: Iterable, fn: FunctionType, *, ignore_not_experiment_objects: bool = True, **kwargs) -> None:
    """
    Call method with each experiment as argument.

    Arguments
    ---------
    experiments: Iterable
        Iterable object of experiments.
    fn: FunctionType
        Method which should be called with each experiment.
    ignore_not_experiment_objects: bool, optional
        Ignore objects which are not an experiment, otherwise raise error.
    **kwargs: dict
        Additional arguments for method `fn`.
    """
    items = experiments.values() if isinstance(experiments, dict) else experiments
    for experiment in items:
        if isinstance(experiment, Experiment):
            fn(experiment, **kwargs)
            continue
        if isinstance(experiment, Iterable):
            foreach_experiment(experiment, fn, ignore_not_experiment_objects=ignore_not_experiment_objects)
            continue
        if not ignore_not_experiment_objects:
            raise ValueError(f"{type(experiment)} is not an Experiment.")


def load_experiment_config() -> (dict, dict):
    """
    Load experiments from config (mlflow_experiments.json).

    Returns
    -------
    dict
        Experiments dictionary.
    """
    def get_experiments_dict(d: dict, *, basepath: str = '.') -> dict:
        """
        Get an experiments dictionary based on another dictionary with:
        - its value as experiment id and
        - its key path as experiment base path.

        Arguments
        ---------
        d: dict
            Template/Base dictionary.
        basepath: str
            Base path of the values or key path (value) prefix.

        Returns
        -------
        dict
            Experiments dictionary with same structure as dictionary d.
        """
        out = dict()
        for key in d.keys():
            if not isinstance(d[key], dict):
                logging.error('Experiment config structure unknown.')
                exit(1)
            if '__id' in d[key]:
                sort = d[key]['sort'] if 'sort'in d[key] else None
                out[key] = Experiment(d[key]['__id'], f"{path.join(basepath, key)}.csv", sort=sort)
                continue

            out[key] = get_experiments_dict(d[key], basepath=path.join(basepath, key))
        return out

    with open('./scripts/mlflow_experiments.json') as f:
        ids = json.loads(f.read())

    return get_experiments_dict(ids, basepath='./data/experiments')


def _pretty_print_json(*args, skip_not_serializable_items: bool = True, **kwargs) -> None:
    """
    Print any object as pretty and colorful JSON string.

    Arguments
    ---------
    *args: tuple
        List of objects to format, colorize and print.
    skip_not_serializable_items: bool, optional
        Skip not serializable items.
    **kwargs: dict
        Custom `json.dumps` arguments.
    """
    class SkipNotSerializableEncoder(json.JSONEncoder):
        def default(cls, obj):
            return 'Skip not serializable item!'

    from pygments import highlight
    from pygments.formatters import TerminalFormatter
    from pygments.lexers import JsonLexer

    if not 'indent' in kwargs: kwargs['indent'] = 4
    if skip_not_serializable_items and not 'cls' in kwargs: kwargs['cls'] = SkipNotSerializableEncoder

    for obj in args:
        formatted_json = json.dumps(obj, **kwargs)
        colored_json = highlight(formatted_json, JsonLexer(), TerminalFormatter())
        print(colored_json, end='')
