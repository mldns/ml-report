#!/usr/bin/env python
# cspell:words klearn
# cspell:enableCompoundWords
############################################################
# Merge tensorflow and/or klearn data and export them.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import pandas as pd

from scripts.experiments import Experiment, foreach_experiment, load_experiment_config


def main() -> None:
    """
    Main entry for additional csv data tables creation script.
    """
    experiments = load_experiment_config()
    foreach_experiment(experiments, lambda experiment: experiment.load())

    # merge all experiments under tensorflow or klearn
    d = { }
    for df, names, typename in [(
        experiments['tensorflow'],
        list(experiments['tensorflow'].keys()),
        'tensorflow'
    ), (
        experiments['klearn'],
        list(experiments['klearn'].keys()),
        'klearn'
    )]:
        frames = []
        for name in names:
            if 'data' not in df[name] or df[name]['data'] is None:
                continue
            f = df[name]['data'].copy()
            f['experiment'] = name
            frames.append(f)
        d[typename] = pd.concat(frames)

    # merge tensorflow and klearn
    tf = d['tensorflow'].copy()
    tf['experiment'] = tf['experiment'].apply(lambda x: f"tensorflow.{x}")
    klearn = d['klearn'].copy()
    klearn['experiment'] = klearn['experiment'].apply(lambda x: f"klearn.{x}")
    d['tensorflow_klearn'] = pd.concat([tf, klearn])

    # convert to experiment object, sort and export
    for key in d.keys():
        d[key] = Experiment(
            -1,
            f"./data/experiments/{key}.csv",
            data=d[key],
            sort=experiments['tensorflow']['fasttext']['sort']
        )
        d[key].sort()
        d[key].export()


if __name__ == '__main__':
    main()
